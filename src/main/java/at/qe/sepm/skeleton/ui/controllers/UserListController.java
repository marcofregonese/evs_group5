package at.qe.sepm.skeleton.ui.controllers;

import at.qe.sepm.skeleton.model.User;
import at.qe.sepm.skeleton.model.UserRole;
import at.qe.sepm.skeleton.services.UserService;
import java.io.Serializable;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.context.annotation.Scope;
import org.springframework.stereotype.Component;

import java.util.Collection;
import java.util.Set;

/**
 * Controller for the user list view.
 *
 * This class is part of the skeleton project provided for students of the
 * course "Softwaredevelopment and Project Management" offered by the University
 * of Innsbruck.
 */
@Component
@Scope("view")
public class UserListController implements Serializable{

    @Autowired
    private UserService userService;

    /**
     * Returns a list of all users.
     *
     * @return
     */
    public Collection<User> getUsers() {
        return userService.getAllUsers();
    }

    public int getUserCount() {
        return userService.getAllUsers().size();
    }
    
    public Boolean checkRole(Set<UserRole> userRoles, String role){
        switch(role){
            case "ADMIN":
                return userRoles.contains(UserRole.ADMIN);
            case "STUDENT":
                return userRoles.contains(UserRole.STUDENT);
            case "EMPLOYEE":
                return userRoles.contains(UserRole.EMPLOYEE);
        }
        return false;
    }

}
