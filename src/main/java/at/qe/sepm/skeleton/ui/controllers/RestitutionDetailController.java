/**
 * 
 */
package at.qe.sepm.skeleton.ui.controllers;

import java.io.Serializable;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.context.annotation.Scope;
import org.springframework.stereotype.Component;

import at.qe.sepm.skeleton.model.Reservation;
import at.qe.sepm.skeleton.services.RestitutionService;
import at.qe.sepm.skeleton.ui.beans.SessionInfoBean;

/**
 * @author marco
 *
 */

@Component
@Scope("view")
public class RestitutionDetailController implements Serializable{

    @Autowired
    private RestitutionService restitutionService;
    
    @Autowired
    private SessionInfoBean sessionInfoBean;
    
    public Reservation confirmRestitution(Reservation reservation) {
		return restitutionService.confirmRestitution(reservation, sessionInfoBean.getCurrentUserName());
		
    }
}
