package at.qe.sepm.skeleton.ui.controllers;

import at.qe.sepm.skeleton.model.LabDevice;
import at.qe.sepm.skeleton.model.LabDeviceGroup;
import at.qe.sepm.skeleton.model.OpeningTime;
import at.qe.sepm.skeleton.model.Reservation;
import at.qe.sepm.skeleton.model.User;
import at.qe.sepm.skeleton.services.MailService;
import at.qe.sepm.skeleton.services.LabDeviceGroupService;
import at.qe.sepm.skeleton.services.LabDeviceService;
import at.qe.sepm.skeleton.services.OpeningTimeService;
import at.qe.sepm.skeleton.services.ReservationService;
import at.qe.sepm.skeleton.ui.beans.SessionInfoBean;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.context.annotation.Scope;
import org.springframework.stereotype.Component;
import org.springframework.stereotype.Controller;

import javax.annotation.ManagedBean;
import java.io.Serializable;
import java.time.DayOfWeek;
import java.time.Duration;
import java.time.LocalDate;
import java.time.LocalDateTime;
import java.time.LocalTime;
import java.time.ZoneId;
import java.util.ArrayList;
import java.util.Calendar;
import java.util.Date;
import java.util.List;
import java.util.TimeZone;
import java.text.DateFormat;
import java.text.SimpleDateFormat;
import javax.faces.application.FacesMessage;
import javax.faces.context.FacesContext;
import org.primefaces.model.timeline.TimelineEvent;
import org.primefaces.model.timeline.TimelineModel;

/**
 * Created by ${Marc} on ${09.01.2019}.
 */

@Component
@ManagedBean
@Controller
@Scope("view")
public class NewReservationController implements Serializable{

    private static final long serialVersionUID = -5544007632424905922L;
    @Autowired
    private ReservationService reservationService;
    @Autowired
    private LabDeviceService labDeviceService;
    @Autowired
    private LabDeviceGroupService labDeviceGroupService;
    @Autowired
    private OpeningTimeService openingTimeService;
    @Autowired
    private SessionInfoBean sessionInfoBean;
    @Autowired
    private MailService mailService;
    
    private List<LabDevice> labDevices;
    private List<LabDevice> allLabDevices;
    private TimelineModel timeLineModel;  
    private LabDevice labDevice;
    private Date startDate;
    private Date endDate;
    private Date reservationStartDate;
    private Date reservationEndDate;
    private Boolean addEnabled;
    private Date minStartDate;
    private Date minEndDate;
    private String startColor;
    private String endColor;
    private String annotation;
    private LabDeviceGroup labDeviceGroup;
    private Integer maxDuration;
    
    public NewReservationController(){
        labDevices = new ArrayList<>();
        labDevice = new LabDevice();
        allLabDevices = new ArrayList<>();
        reservationStartDate = new Date();
        reservationEndDate = reservationStartDate;
        minStartDate = new Date();
        minEndDate = new Date();
        startColor = "black";
        endColor = "black";
        addEnabled = false;
    }
    
    public void initMultiselectReservation(){
        reservationStartDate = new Date();
        LocalDateTime ldt = LocalDateTime.ofInstant(reservationStartDate.toInstant(), ZoneId.of("CET"));
        while(reservationService.checkIfDateIsNotAHoliday(ldt.toLocalDate()) == false || ldt.getDayOfWeek() == DayOfWeek.SATURDAY || ldt.getDayOfWeek() == DayOfWeek.SUNDAY){
            ldt = ldt.plusDays(1);
        }
        ldt = ldt.withHour(5);
        ldt = ldt.withMinute(0);
        reservationStartDate = Date.from(ldt.atZone(ZoneId.of("CET")).toInstant());
        reservationEndDate = reservationStartDate;
        minStartDate = reservationStartDate;
        minEndDate = reservationStartDate;
        startColor = "black";
        endColor = "black";
        addEnabled = false;
        labDevices.clear();
        allLabDevices.clear();
        annotation = "";
    }
    
    public LabDevice getLabDevice() {
        return labDevice;
    }
    
    public void setLabDevice(LabDevice labDevice) {
        this.labDevice = labDevice;
        labDeviceGroup = null;
        doReloadLabDevice();
        createTimeLine();
    }

    public LabDeviceGroup getLabDeviceGroup() {
        return labDeviceGroup;
    }

    public void setLabDeviceGroup(LabDeviceGroup labDeviceGroup) {
        this.labDeviceGroup = labDeviceGroup;
        doReloadLabDeviceGroup();
        createTimeLine();
    }
    
    private void doReloadLabDeviceGroup(){
        labDeviceGroup = labDeviceGroupService.getLabDeviceGroupById(labDeviceGroup.getId());
        labDevices.clear();
        labDeviceGroup.getLabDevices().forEach(x -> labDevices.add(x));
        maxDuration = labDevices.stream().mapToInt(LabDevice::getMaxDurationInHours).min().getAsInt();
        init();
    }
    
    private void doReloadLabDevice(){
        labDevice = labDeviceService.getLabDeviceById(labDevice.getId());
        labDevices.clear();
        labDevices.add(labDevice);
        maxDuration = labDevice.getMaxDurationInHours();
        init();
    }
    
    private void init(){
        Date d = new Date();
        Calendar c = Calendar.getInstance(TimeZone.getTimeZone("CET"));
        c.setTime(d);
        c.add(Calendar.HOUR_OF_DAY, 1);
        c.set(Calendar.MINUTE, 0);
        c.set(Calendar.SECOND, 0);
        c.set(Calendar.MILLISECOND, 0);
        d = c.getTime();
        reservationStartDate = calculateNextDateTime(d);
        if(checkIfDateDisruptNoReservations(reservationStartDate) == false){
            FacesContext.getCurrentInstance().addMessage("reservation", new FacesMessage(FacesMessage.SEVERITY_INFO, "Startdatum:", "Startdatum steht in konflikt mit einer anderen Reservierung. Betrachten Sie zur Hilfe die Timeline."));
            startColor = "red";
            endColor = "red";
        }
        else{ 
            startColor = "black";
        endColor = "black";
        }
        reservationEndDate = reservationStartDate;
        minStartDate = reservationStartDate;
        minEndDate = reservationStartDate;
        addEnabled = false;
        annotation = "";
    }
    
    private Date calculateNextDateTime(Date fromDate){
        Calendar c = Calendar.getInstance(TimeZone.getTimeZone("CET"));
        c.setTime(fromDate);
        int startday = c.get(Calendar.DAY_OF_WEEK);
        int addDays = 0;
        Boolean search = true;
        OpeningTime ot = null;
        while(search){
            ot = getMinOpenignTimeFromLabs(startday);
            if(ot != null && ot.getId() == null) ot = null;
            if(ot != null && ot.getOpenTime().before(ot.getCloseTime()) && ot.getOpenTime() != ot.getCloseTime()){
                if(addDays == 0){
                    LocalTime openTime = LocalDateTime.ofInstant(ot.getOpenTime().toInstant(), ZoneId.of("CET")).toLocalTime();
                    LocalTime closeTime = LocalDateTime.ofInstant(ot.getCloseTime().toInstant(), ZoneId.of("CET")).toLocalTime();
                    LocalTime aktTime = LocalDateTime.ofInstant(fromDate.toInstant(), ZoneId.of("CET")).toLocalTime();
                    if(openTime.isAfter(aktTime)
                            && reservationService.checkIfDateIsNotAHoliday(LocalDateTime.ofInstant(fromDate.toInstant(), ZoneId.of("CET")).toLocalDate())){
                        c.set(Calendar.HOUR_OF_DAY, openTime.getHour());
                        c.set(Calendar.MINUTE, openTime.getMinute());
                        return c.getTime();
                    }
                    if(closeTime.isAfter(aktTime) && closeTime != aktTime
                            && reservationService.checkIfDateIsNotAHoliday(LocalDateTime.ofInstant(fromDate.toInstant(), ZoneId.of("CET")).toLocalDate())) 
                        return fromDate;
                }
                else{
                    c.add(Calendar.DATE, addDays);
                    LocalTime openTime = LocalDateTime.ofInstant(ot.getOpenTime().toInstant(), ZoneId.of("CET")).toLocalTime();
                    c.set(Calendar.HOUR_OF_DAY, openTime.getHour());
                    c.set(Calendar.MINUTE, openTime.getMinute());
                    if(reservationService.checkIfDateIsNotAHoliday(LocalDateTime.ofInstant(c.getTime().toInstant(), ZoneId.of("CET")).toLocalDate())) 
                        return c.getTime();
                }
            }
            if (search == true){ 
                startday++;
                if(startday > 6){ 
                    startday = 2;
                    addDays = addDays + 2;
                }
                addDays++;
            }
        }
        return fromDate;
    }
    
    private OpeningTime getMinOpenignTimeFromLabs(Integer startday){
        OpeningTime ret = openingTimeService.getOpeningTime(labDevices.get(0).getLaboratory(), getDayOfInteger(startday));
        if(ret.getId() == null) return null;
        else{
            OpeningTime ot;
            LocalTime ltret = LocalDateTime.ofInstant(ret.getOpenTime().toInstant(), ZoneId.of("CET")).toLocalTime();
            LocalTime ltot;
            for(int i = 1; i < labDevices.size(); i++){
                ot = openingTimeService.getOpeningTime(labDevices.get(i).getLaboratory(), getDayOfInteger(startday));
                if(ot == null) return null;
                else{
                    ltot = LocalDateTime.ofInstant(ot.getOpenTime().toInstant(), ZoneId.of("CET")).toLocalTime();
                    if(ltot.isAfter(ltret)){
                        ret = ot;
                        ltret = LocalDateTime.ofInstant(ret.getOpenTime().toInstant(), ZoneId.of("CET")).toLocalTime();
                    }
                }
            }
            return ret;
        }
    }
    
    private String getDayOfInteger(Integer day){
        switch (day){
            case Calendar.MONDAY:
                return "MON";
            case Calendar.TUESDAY:
                return "TUE";
            case Calendar.WEDNESDAY:
                return "WED";
            case Calendar.THURSDAY:
                return "THU";
            case Calendar.FRIDAY:
                return "FRI";
        }
        return "";
    }

    public List<LabDevice> getLabDevices() {
        return labDevices;
    }

    public void setLabDevices(List<LabDevice> labDevices) {
        this.labDevices = labDevices;
    }
    
    private void createTimeLine() {  
        timeLineModel = new TimelineModel();  
        Calendar calendar = Calendar.getInstance(TimeZone.getTimeZone("CET"));
        startDate = new Date();
        endDate = new Date();
        Date start = new Date();
        Date end = new Date(); 
        
        if(reservationStartDate != reservationEndDate){
            TimelineEvent event = new TimelineEvent("Neue Reservierung", reservationStartDate, reservationEndDate, false, "Ihre Reservierungen", "available");  
            timeLineModel.add(event); 
            end = reservationEndDate;
            if(end.after(endDate)) endDate = end;
        }
        User user = sessionInfoBean.getCurrentUser();
        for (LabDevice ld : labDevices) {
            List<Reservation> reservations = reservationService.getAllActiveReservationsFromLabDevice(ld.getId());
            for(Reservation res : reservations){
                calendar.clear();
                calendar.set(res.getStartDate().getYear(),
                        res.getStartDate().getMonthValue() - 1,
                        res.getStartDate().getDayOfMonth(),
                        res.getStartTime().getHour(), 
                        res.getStartTime().getMinute(), 
                        res.getStartTime().getSecond());
                start = calendar.getTime();
                calendar.clear();
                calendar.set(res.getEndDate().getYear(),
                        res.getEndDate().getMonthValue() - 1,
                        res.getEndDate().getDayOfMonth(),
                        res.getEndTime().getHour(), 
                        res.getEndTime().getMinute(), 
                        res.getEndTime().getSecond());
                end = calendar.getTime();
                TimelineEvent event;
                if(res.getUserTag().getUsername().equals(user.getUsername())) event = new TimelineEvent("Reserviert", start, end, false, "Ihre Reservierungen", "maybe");
                else event = new TimelineEvent("Reserviert", start, end, false, ld.getName(), "unavailable");   
                timeLineModel.add(event);  
                if(end.after(endDate)) endDate = end;
            }
        }
        calendar.clear();
        calendar.setTime(endDate); 
        calendar.add(Calendar.DATE, 2);
        endDate = calendar.getTime(); 
    }
    
    private Boolean checkIfDateDisruptNoReservations(Date date){
        LocalDateTime ldt = LocalDateTime.ofInstant(date.toInstant(), ZoneId.of("CET"));
        Boolean ok = true;
        for(LabDevice ld : labDevices){
            ok = reservationService.checkIfDateDisruptNoReservations(ldt.toLocalDate(), ldt.toLocalTime(), ld);
            if(ok == false) return ok;
        }
        return ok;
    }
    
    public void startDateSelected(){
        Date old = reservationStartDate;
        
        reservationStartDate = calculateNextDateTime(reservationStartDate);
        
        if(old.before(reservationStartDate)) FacesContext.getCurrentInstance().addMessage("reservation", new FacesMessage(FacesMessage.SEVERITY_INFO, "Startdatum:", "Ihr eingegebenes Startdatum wurde an die Öffnungszeiten des Labores angepasst"));
        
        if(checkIfDateDisruptNoReservations(reservationStartDate) == false){
            FacesContext.getCurrentInstance().addMessage("reservation", new FacesMessage(FacesMessage.SEVERITY_INFO, "Startdatum:", "Ihr eingegebenes Startdatum steht in konflikt mit einer anderen Reservierung. Betrachten Sie zur Hilfe die Timeline."));
            startColor = "red";
        }
        else startColor = "black";
        if(reservationEndDate.before(reservationStartDate)){
            reservationEndDate = reservationStartDate;
            endColor = startColor;
        }
        else{
            int days = (int)(maxDuration / 24);
            int hours = maxDuration - (days * 24);
            Calendar cFrom = Calendar.getInstance(TimeZone.getTimeZone("CET"));
            cFrom.setTime(reservationStartDate);
            cFrom.add(Calendar.DATE, days);
            cFrom.add(Calendar.HOUR_OF_DAY, hours);
            Calendar cTo = Calendar.getInstance(TimeZone.getTimeZone("CET"));
            cTo.setTime(reservationEndDate);
            if(cTo.after(cFrom)) reservationEndDate = cFrom.getTime();
            
            reservationEndDate = calculateNextDateTime(reservationEndDate);
            
            if(checkIfDateDisruptNoReservations(reservationEndDate) == false){
                FacesContext.getCurrentInstance().addMessage("reservation", new FacesMessage(FacesMessage.SEVERITY_INFO, "Enddatum:", "Das vom System bezüglich maximaler Ausleihdauer und Öffnungszeiten des Labores angepasste Enddatum stört eine andere Reservierung. Betrachten Sie zur Hilfe die Timeline."));
                endColor = "red";
            }
            else endColor = "black";
        }
        minEndDate = reservationStartDate;
        createTimeLine();
        checkAddEnabled();
    }
    
    public void endDateSelected(){
        boolean angepasst = false;
        if(reservationEndDate.before(reservationStartDate)) reservationEndDate = reservationStartDate;
        else{
            int days = (int)(maxDuration / 24);
            int hours = maxDuration - (days * 24);
            Calendar cFrom = Calendar.getInstance(TimeZone.getTimeZone("CET"));
            cFrom.setTime(reservationStartDate);
            cFrom.add(Calendar.DATE, days);
            cFrom.add(Calendar.HOUR_OF_DAY, hours);
            Calendar cTo = Calendar.getInstance(TimeZone.getTimeZone("CET"));
            cTo.setTime(reservationEndDate);
            if(cTo.after(cFrom)){ 
                reservationEndDate = cFrom.getTime();
                angepasst = true;
            }
            reservationEndDate = calculateNextDateTime(reservationEndDate);
        
        }
        if(checkIfDateDisruptNoReservations(reservationEndDate) == false){
            FacesContext.getCurrentInstance().addMessage("reservation", new FacesMessage(FacesMessage.SEVERITY_INFO, "Enddatum:", "Das vom System bezüglich maximaler Ausleihdauer und Öffnungszeiten des Labores angepasste Enddatum stört eine andere Reservierung. Betrachten Sie zur Hilfe die Timeline."));
            endColor = "red";
        }
        else{ 
            if (angepasst == true) FacesContext.getCurrentInstance().addMessage("reservation", new FacesMessage(FacesMessage.SEVERITY_INFO, "Enddatum:", "Das Enddatum wurde an die maximal Ausleihdauer angepasst."));
            endColor = "black";
        }
        createTimeLine();
        checkAddEnabled();
    }
    
    public void doSaveReservation(){
        for(LabDevice ld : labDevices){
            Reservation reservation = new Reservation();
            reservation.setAnnotation(annotation);
            reservation.setLabDeviceTag(ld);
            reservation.setUserTag(sessionInfoBean.getCurrentUser());
            LocalDateTime start = LocalDateTime.ofInstant(reservationStartDate.toInstant(),ZoneId.of("CET"));
            LocalDateTime end = LocalDateTime.ofInstant(reservationEndDate.toInstant(),ZoneId.of("CET"));
            reservation.setStartDate(start.toLocalDate());
            reservation.setEndDate(end.toLocalDate());
            reservation.setStartTime(start.toLocalTime());
            reservation.setEndTime(end.toLocalTime());
            reservation.setLabDeviceGroup(labDeviceGroup);
            reservationService.saveReservation(reservation);
        }
        
        //Send Mail to User
        String usermail = sessionInfoBean.getCurrentUser().getEmail();
        if(usermail != null && usermail.isEmpty() == false){
            DateFormat dateFormat = new SimpleDateFormat("dd.MM.yyyy HH:mm");
            if(labDeviceGroup == null){
                String mailtext = "Bestätigung Ihrer Gerätereservierung vom " + dateFormat.format(new Date()) + "\n\n";
                mailtext += "Start Ihrer Reservierung: " + dateFormat.format(reservationStartDate) + "\n";
                mailtext += "Ende Ihrer Reservierung: " + dateFormat.format(reservationEndDate) + "\n\n";
                if(labDevices.size() > 1) mailtext += "Ihre Reservierung beinhaltet folgende Geräte (Labor - Standort):\n";
                else mailtext += "Ihre Reservierung beinhaltet folgendes Gerät (Labor - Standort):\n";
                for(LabDevice ld : labDevices) {
                    mailtext += "\t"+ "- " + ld.getName() + " (" + ld.getLaboratory().getLaboratoryName() + " - " + ld.getLocation() + ")\n";
                }
                mailtext += "\nMit freundlichen Grüßen\nUIBK Geräte Team";
                mailService.SendMail(usermail, "UIBK Geräte: Bestätigung Ihrer neuen Gerätereservierung", mailtext);
            }
            else{ 
                String mailtext = "Bestätigung Ihrer Gruppenreservierung vom " + dateFormat.format(new Date()) + "\n\n";
                mailtext += "Start Ihrer Reservierung: " + dateFormat.format(reservationStartDate) + "\n";
                mailtext += "Ende Ihrer Reservierung: " + dateFormat.format(reservationEndDate) + "\n\n";
                mailtext += "Ihre Reservierung beinhaltet die Gerätegruppe " + labDeviceGroup.getName() + " mit folgenden Geräten (Labor - Standort):\n";
                for(LabDevice ld : labDevices) {
                    mailtext += "\t" + "- " + ld.getName() + " (" + ld.getLaboratory().getLaboratoryName() + " - " + ld.getLocation() + ")\n";
                }
                mailtext += "\nMit freundlichen Grüßen\nUIBK Geräte Team";
                mailService.SendMail(usermail, "UIBK Geräte: Bestätigung Ihrer neuen Gruppenreservierung", mailtext);
            }
        }
    }
    
    public void checkAddEnabled(){
        if(startColor.equals("black") && endColor.equals("black") && reservationStartDate.equals(reservationEndDate) == false){
            LocalDateTime start = LocalDateTime.ofInstant(reservationStartDate.toInstant(), ZoneId.of("CET"));
            LocalDateTime end = LocalDateTime.ofInstant(reservationEndDate.toInstant(), ZoneId.of("CET"));
            if(annotation == null || annotation.isEmpty()) addEnabled = false;
            else{
                for (LabDevice ld : labDevices){
                    if(reservationService.isReservationBetweenDateSpan(start.toLocalDate(), start.toLocalTime(), end.toLocalDate(), end.toLocalTime(), labDevice.getId())){
                        addEnabled = false;
                        FacesContext.getCurrentInstance().addMessage("reservation", new FacesMessage(FacesMessage.SEVERITY_INFO, "Achtung:", "Ihr Reservierungszeitraum beinhaltet eine andere Reservierung. Betrachten Sie zur Hilfe die Timeline."));
                        break;
                    }
                    else { 
                        addEnabled = true;
                    }
                }
            }
        }
        else addEnabled = false;
    }
    
    public void checkMultiAddEnabled(){
        if(allLabDevices.size() > 0 && annotation != null && annotation.isEmpty() == false && reservationStartDate.before(reservationEndDate)) addEnabled = true;
        else addEnabled = false;
    }
    
    public void multiStartDateSelected(){
        labDevices.clear();
        LocalDateTime ldt = LocalDateTime.ofInstant(reservationStartDate.toInstant(), ZoneId.of("CET"));
        while(reservationService.checkIfDateIsNotAHoliday(ldt.toLocalDate()) == false || ldt.getDayOfWeek() == DayOfWeek.SATURDAY || ldt.getDayOfWeek() == DayOfWeek.SUNDAY){
            ldt = ldt.plusDays(1);
        }
        reservationStartDate = Date.from(ldt.atZone(ZoneId.of("CET")).toInstant());
        if(reservationStartDate.after(reservationEndDate)){
            labDevices.clear();
            reservationEndDate = reservationStartDate;
        }
        else if(reservationStartDate.equals(reservationEndDate) == false){
            allLabDevices = reservationService.getReservableLabDevices(reservationStartDate, reservationEndDate, calculateMinMaxDuration());
        }
        checkMultiAddEnabled();
    }
    
    public void multiEndDateSelected(){
        labDevices.clear();
        LocalDateTime ldt = LocalDateTime.ofInstant(reservationEndDate.toInstant(), ZoneId.of("CET"));
        while(reservationService.checkIfDateIsNotAHoliday(ldt.toLocalDate()) == false || ldt.getDayOfWeek() == DayOfWeek.SATURDAY || ldt.getDayOfWeek() == DayOfWeek.SUNDAY){
            ldt = ldt.plusDays(1);
        }
        reservationEndDate = Date.from(ldt.atZone(ZoneId.of("CET")).toInstant());
        allLabDevices = reservationService.getReservableLabDevices(reservationStartDate, reservationEndDate, calculateMinMaxDuration());
        checkMultiAddEnabled();
    }
    
    public Integer calculateMinMaxDuration(){
        LocalDateTime ldtStart = LocalDateTime.ofInstant(reservationStartDate.toInstant(), ZoneId.of("CET"));
        LocalDateTime ldtEnd = LocalDateTime.ofInstant(reservationEndDate.toInstant(), ZoneId.of("CET"));
        int duration = (int)(Duration.between(ldtStart, ldtEnd).toHours()) + 1;
        for(LocalDate date = ldtStart.toLocalDate(); date.isBefore(ldtEnd.toLocalDate()); date = date.plusDays(1)){
            if(date.getDayOfWeek() == DayOfWeek.SATURDAY || date.getDayOfWeek() == DayOfWeek.SUNDAY) duration = duration - 24;
            else if (reservationService.checkIfDateIsNotAHoliday(date) == false)  duration = duration - 24;
        }
        return duration;
    }

    public TimelineModel getTimeLineModel() {
        return timeLineModel;
    }

    public void setTimeLineModel(TimelineModel timeLineModel) {
        this.timeLineModel = timeLineModel;
    }

    public Date getStartDate() {
        return startDate;
    }

    public void setStartDate(Date startDate) {
        this.startDate = startDate;
    }

    public Date getEndDate() {
        return endDate;
    }

    public void setEndDate(Date endDate) {
        this.endDate = endDate;
    }

    public Date getReservationStartDate() {
        return reservationStartDate;
    }

    public void setReservationStartDate(Date reservationStartDate) {
        this.reservationStartDate = reservationStartDate;
    }

    public Date getReservationEndDate() {
        return reservationEndDate;
    }

    public void setReservationEndDate(Date reservationEndDate) {
        this.reservationEndDate = reservationEndDate;
    }

    public Boolean getAddEnabled() {
        return addEnabled;
    }

    public void setAddEnabled(Boolean addEnabled) {
        this.addEnabled = addEnabled;
    }

    public Date getMinStartDate() {
        return minStartDate;
    }

    public void setMinStartDate(Date minStartDate) {
        this.minStartDate = minStartDate;
    }

    public Date getMinEndDate() {
        return minEndDate;
    }

    public void setMinEndDate(Date minEndDate) {
        this.minEndDate = minEndDate;
    }
    
    public String getStartColor(){
        return startColor;
    }
    
    public String getEndColor(){
        return endColor;
    }

    public String getAnnotation() {
        return annotation;
    }

    public void setAnnotation(String annotation) {
        this.annotation = annotation;
    }

    public Integer getMaxDuration() {
        return maxDuration;
    }

    public void setMaxDuration(Integer maxDuration) {
        this.maxDuration = maxDuration;
    }

    public List<LabDevice> getAllLabDevices() {
        return allLabDevices;
    }

    public void setAllLabDevices(List<LabDevice> allLabDevices) {
        this.allLabDevices = allLabDevices;
    }
}