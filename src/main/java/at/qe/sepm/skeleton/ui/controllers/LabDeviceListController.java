package at.qe.sepm.skeleton.ui.controllers;

import at.qe.sepm.skeleton.model.LabDevice;
import at.qe.sepm.skeleton.services.LabDeviceService;
import at.qe.sepm.skeleton.services.ReservationService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.context.annotation.Scope;
import org.springframework.stereotype.Component;

import java.io.Serializable;
import java.time.LocalDate;
import java.time.LocalTime;
import java.time.ZoneId;
import java.util.Date;
import java.util.LinkedList;
import java.util.List;

/**
 * Created by ${Marc} on ${05.12.2018}.
 */

@Component
@Scope("session")
public class LabDeviceListController implements Serializable {

    private static final long serialVersionUID = -7830965913689323493L;

    @Autowired
    private LabDeviceService labDeviceService;
    @Autowired
    private ReservationService reservationService;


    private String labDeviceModel;
    private LocalDate startDate;
    private Date tempStartDate;
    private LocalDate endDate;
    private Date tempEndDate;
    private LocalTime startTime;
    private Date tempStartTime;
    private LocalTime endTime;
    private Date tempEndTime;
    private List<LabDevice> filteredAllLabDevices;

    public List<LabDevice> getAllLabDevices() {
        return labDeviceService.getAllLabDevices();
    }

    public List<LabDevice> getAllLabDevicesByName(String name) {
        return labDeviceService.getLabDevicesAllByName(name);
    }

    public List<LabDevice> getAllLabDevicesOfLab(Integer labId) {return labDeviceService.getAllLabDevicesOfLab(labId); }

    public LabDevice getLabDeviceById(int id) {
        return labDeviceService.getLabDeviceById(id);
    }
    
    public void delete(LabDevice labDevice){
        labDeviceService.deleteLabDevice(labDevice);
    }

    private void setDateTimes() {
        startDate = tempStartDate.toInstant().atZone(ZoneId.systemDefault()).toLocalDate();
        endDate = tempEndDate.toInstant().atZone(ZoneId.systemDefault()).toLocalDate();
        startTime =tempStartTime.toInstant().atZone(ZoneId.systemDefault()).toLocalTime();
        endTime = tempEndTime.toInstant().atZone(ZoneId.systemDefault()).toLocalTime();
    }

    public List<LabDevice> getReservableLabDeviceOfModel() {
        setDateTimes();
        List<Integer> IDs = labDeviceService.getAllLabDeviceIds();
        List<LabDevice> labDevicesWithoutReservation = new LinkedList<>();
        for (int i = 0; i < IDs.size(); i++) {
            if (reservationService.getAllReservationsFromLabDeviceId(IDs.get(i)).isEmpty()) {
                labDevicesWithoutReservation.add(labDeviceService.getLabDeviceById(IDs.get(i)));
            }
        }
        labDevicesWithoutReservation.addAll(reservationService.getReservableLabDevicesOfModel(startDate, endDate, startTime, endTime, labDeviceModel));
        return labDevicesWithoutReservation;
    }

    public int getLabDeviceCount() {
        return labDeviceService.getAllLabDevices().size();
    }


    public String getLabDeviceModel() {
        return labDeviceModel;
    }

    public void setLabDeviceModel(String labDeviceModel) {
        this.labDeviceModel = labDeviceModel;
    }

    public LocalDate getStartDate() {
        return startDate;
    }

    public void setStartDate(LocalDate startDate) {
        this.startDate = startDate;
    }

    public Date getTempStartDate() {
        return tempStartDate;
    }

    public void setTempStartDate(Date tempStartDate) {
        this.tempStartDate = tempStartDate;
    }

    public LocalDate getEndDate() {
        return endDate;
    }

    public void setEndDate(LocalDate endDate) {
        this.endDate = endDate;
    }

    public Date getTempEndDate() {
        return tempEndDate;
    }

    public void setTempEndDate(Date tempEndDate) {
        this.tempEndDate = tempEndDate;
    }

    public LocalTime getStartTime() {
        return startTime;
    }

    public void setStartTime(LocalTime startTime) {
        this.startTime = startTime;
    }

    public Date getTempStartTime() {
        return tempStartTime;
    }

    public void setTempStartTime(Date tempStartTime) {
        this.tempStartTime = tempStartTime;
    }

    public LocalTime getEndTime() {
        return endTime;
    }

    public void setEndTime(LocalTime endTime) {
        this.endTime = endTime;
    }

    public Date getTempEndTime() {
        return tempEndTime;
    }

    public void setTempEndTime(Date tempEndTime) {
        this.tempEndTime = tempEndTime;
    }

	public List<LabDevice> getFilteredAllLabDevices() {
		return filteredAllLabDevices;
	}

	public void setFilteredAllLabDevices(List<LabDevice> filteredAllLabDevices) {
		this.filteredAllLabDevices = filteredAllLabDevices;
	}
}
