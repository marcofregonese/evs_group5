/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package at.qe.sepm.skeleton.ui.controllers;

import at.qe.sepm.skeleton.model.Laboratory;
import at.qe.sepm.skeleton.services.LaboratoryService;
import java.io.Serializable;
import javax.faces.application.FacesMessage;
import javax.faces.context.FacesContext;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.context.annotation.Scope;
import org.springframework.stereotype.Component;

/**
 *
 * @author Manuel
 */
@Component
@Scope("view")
public class LaboratoryAddController implements Serializable {
    
    @Autowired
    private LaboratoryService laboratoryService;
    
    private Laboratory laboratory;
    private Boolean addEnabled;
    private Boolean exists;
    
    public LaboratoryAddController() {
        laboratory = new Laboratory();
        addEnabled = false;
    }
    
    public void inputChanged(Boolean name){
        if(name == true && laboratory.getLaboratoryName()!= null){ 
            Laboratory l = laboratoryService.getLaborytorybyName(laboratory.getLaboratoryName());
            if(l != null) {
                exists = true;
                FacesContext.getCurrentInstance().addMessage("name", new FacesMessage(FacesMessage.SEVERITY_ERROR, "Fehler!", "Dieser Laborname exestiert bereits!"));
            }
            else exists = false;
        }
        addEnabled = (laboratory.getLaboratoryName() != null && laboratory.getLaboratoryName().isEmpty() == false && exists == false);
    }
    
    public void save(){
        laboratory = laboratoryService.saveLaboratory(laboratory);
    }

    public Laboratory getLaboratory() {
        return laboratory;
    }

    public void setLaboratory(Laboratory laboratory) {
        this.laboratory = laboratory;
    }

    public Boolean getAddEnabled() {
        return addEnabled;
    }

    public void setAddEnabled(Boolean addEnabled) {
        this.addEnabled = addEnabled;
    }
}
