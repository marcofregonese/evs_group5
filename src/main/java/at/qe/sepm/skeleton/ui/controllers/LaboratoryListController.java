/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package at.qe.sepm.skeleton.ui.controllers;

import at.qe.sepm.skeleton.model.Laboratory;
import at.qe.sepm.skeleton.services.LaboratoryService;
import java.io.Serializable;
import java.util.List;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.context.annotation.Scope;
import org.springframework.stereotype.Component;

/**
 *
 * @author Manuel
 */
@Component
@Scope("view")
public class LaboratoryListController implements Serializable {
    
    @Autowired
    private LaboratoryService laboratoryService;

    public List<Laboratory> getAllLaboratories() {
        return laboratoryService.getAllLaboratories();
    }

    public List<Laboratory> getAllLaboratoriesByName(String name) {
        return laboratoryService.getAllLaborytorybyName(name);
    }
}
