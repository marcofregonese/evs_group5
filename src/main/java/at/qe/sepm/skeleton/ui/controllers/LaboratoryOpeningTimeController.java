/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package at.qe.sepm.skeleton.ui.controllers;

import at.qe.sepm.skeleton.model.Laboratory;
import at.qe.sepm.skeleton.model.OpeningTime;
import at.qe.sepm.skeleton.services.LaboratoryService;
import at.qe.sepm.skeleton.services.OpeningTimeService;
import java.io.Serializable;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.context.annotation.Scope;
import org.springframework.stereotype.Component;

/**
 *
 * @author Manuel
 */
@Component
@Scope("view")
public class LaboratoryOpeningTimeController implements Serializable{
    
    @Autowired
    private OpeningTimeService openingTimeService;
    
    @Autowired
    private LaboratoryService laboratoryService;
    
    private Laboratory laboratory;
    private OpeningTime mon = new OpeningTime();
    private OpeningTime tue = new OpeningTime();
    private OpeningTime wed = new OpeningTime();
    private OpeningTime thu = new OpeningTime();
    private OpeningTime fri = new OpeningTime();
    
    public void setLaboratory(Laboratory laboratory) {
        this.laboratory = laboratory;
        doReloadLaboratory();
        mon = openingTimeService.getOpeningTime(this.laboratory, "MON");
        tue = openingTimeService.getOpeningTime(this.laboratory, "TUE");
        wed = openingTimeService.getOpeningTime(this.laboratory, "WED");
        thu = openingTimeService.getOpeningTime(this.laboratory, "THU");
        fri = openingTimeService.getOpeningTime(this.laboratory, "FRI");
    }
    
    public OpeningTime getOpeningTimeForLabrotoryAndDay(Laboratory laboratory, String dayOfWeek){
        return openingTimeService.getOpeningTime(laboratory, dayOfWeek);
    }
    
    public void save(){
        mon = openingTimeService.save(mon);
        tue = openingTimeService.save(tue);
        wed = openingTimeService.save(wed);
        thu = openingTimeService.save(thu);
        fri = openingTimeService.save(fri);
    }
    
    public void doReloadLaboratory(){
        this.laboratory = laboratoryService.getLaboratoryById(this.laboratory.getId());
    }
    
    public void doDeleteLaboratory() {
        this.laboratoryService.deleteLaboratory(laboratory);
        laboratory = null;
    }

    public Laboratory getLaboratory() {
        return laboratory;
    }

    public OpeningTimeService getOpeningTimeService() {
        return openingTimeService;
    }

    public void setOpeningTimeService(OpeningTimeService openingTimeService) {
        this.openingTimeService = openingTimeService;
    }

    public OpeningTime getMon() {
        return mon;
    }

    public void setMon(OpeningTime mon) {
        this.mon = mon;
    }

    public OpeningTime getTue() {
        return tue;
    }

    public void setTue(OpeningTime tue) {
        this.tue = tue;
    }

    public OpeningTime getWed() {
        return wed;
    }

    public void setWed(OpeningTime wed) {
        this.wed = wed;
    }

    public OpeningTime getThu() {
        return thu;
    }

    public void setThu(OpeningTime thu) {
        this.thu = thu;
    }

    public OpeningTime getFri() {
        return fri;
    }

    public void setFri(OpeningTime fri) {
        this.fri = fri;
    }
}
