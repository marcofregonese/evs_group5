package at.qe.sepm.skeleton.ui.controllers;

import at.qe.sepm.skeleton.model.Reservation;
import at.qe.sepm.skeleton.model.UserRole;
import at.qe.sepm.skeleton.ui.beans.SessionInfoBean;
import at.qe.sepm.skeleton.services.ReservationService;
import at.qe.sepm.skeleton.services.LabDeviceService;
import at.qe.sepm.skeleton.services.LabDeviceGroupService;
import at.qe.sepm.skeleton.services.UserService;
import java.io.Serializable;
import java.time.LocalDateTime;
import java.time.ZoneId;
import java.util.Date;
import java.util.List;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.context.annotation.Scope;
import org.springframework.stereotype.Component;

/**
 *
 * @author Manuel
 */
@Component
@Scope("view")
public class CountController implements Serializable{

    @Autowired
    private SessionInfoBean sessionInfoBean;
    
    @Autowired
    private ReservationService reservationService;
    
    @Autowired
    private LabDeviceService labDeviceService;
    
    @Autowired
    private LabDeviceGroupService labDeviceGroupService;
    
    @Autowired
    private UserService userService;
    
    private Integer countAllReservations;
    private Integer countRunReservations;
    private Integer countOpenReservations;
    private Integer countDueReservations;
    private Integer countDoneReservations;
    private Integer countLabDevices;
    private Integer countLabDeviceGroups;
    private Integer countUsers;
    private Integer prozRunReservation;
    private Integer prozOpenReservations;
    private Integer prozDueReservations;
    private Integer prozDoneReservations;
    
    private void calculateCounts(){
        List<Reservation> reservations;
        countLabDevices = labDeviceService.getAllLabDevices().size();
        countUsers = userService.getAllUsers().size();
        if(sessionInfoBean.getCurrentUser().getRoles().contains(UserRole.ADMIN)){ 
            reservations = reservationService.getReservations();
            countLabDeviceGroups = labDeviceGroupService.getAllLabDeviceGroups().size();
        }
        else {
            reservations = reservationService.getReservationsFromUsername(sessionInfoBean.getCurrentUserName());
            countLabDeviceGroups = labDeviceGroupService.getLabDeviceGroupsOfUser(sessionInfoBean.getCurrentUser()).size();
        }
        countAllReservations = reservations.size();
        countRunReservations = 0;
        countOpenReservations = 0;
        countDueReservations = 0;
        countDoneReservations = 0;
        
        Date aktDate = new Date();
        for(Reservation reservation : reservations){
            Date start = Date.from(LocalDateTime.of(reservation.getStartDate(), reservation.getStartTime()).atZone(ZoneId.of("CET")).toInstant());
            Date end = Date.from(LocalDateTime.of(reservation.getEndDate(), reservation.getEndTime()).atZone(ZoneId.of("CET")).toInstant());
            if(reservation.getRestitutionDate() != null) countDoneReservations++;
            else if(start.after(aktDate)) countOpenReservations++;
            else if(start.before(aktDate) && end.after(aktDate)) countRunReservations++; 
            else if(end.before(aktDate)) countDueReservations++;
        }
        
        prozRunReservation = 0;
        prozOpenReservations = 0;
        prozDueReservations = 0;
        prozDoneReservations = 0;
        
        if(countAllReservations != 0){
            prozRunReservation = (int)(((double)countRunReservations / (double)countAllReservations) * (double)100);
            prozOpenReservations = (int)(((double)countOpenReservations / (double)countAllReservations) * (double)100);
            prozDueReservations = (int)(((double)countDueReservations / (double)countAllReservations) * (double)100);
            prozDoneReservations = (int)(((double)countDoneReservations / (double)countAllReservations) * (double)100);
        }
    }

    public Integer getCountAllReservations() {
        if(countAllReservations == null) calculateCounts();
        return countAllReservations;
    }

    public void setCountAllReservations(Integer countAllReservations) {
        this.countAllReservations = countAllReservations;
    }

    public Integer getCountRunReservations() {
        if(countRunReservations == null) calculateCounts();
        return countRunReservations;
    }

    public void setCountRunReservations(Integer countRunReservations) {
        this.countRunReservations = countRunReservations;
    }

    public Integer getCountOpenReservations() {
        if(countOpenReservations == null) calculateCounts();
        return countOpenReservations;
    }

    public void setCountOpenReservations(Integer countOpenReservations) {
        this.countOpenReservations = countOpenReservations;
    }

    public Integer getCountDueReservations() {
        if(countDueReservations == null) calculateCounts();
        return countDueReservations;
    }

    public void setCountDueReservations(Integer countDueReservations) {
        this.countDueReservations = countDueReservations;
    }

    public Integer getCountDoneReservations() {
        if(countDoneReservations == null) calculateCounts();
        return countDoneReservations;
    }

    public void setCountDoneReservations(Integer countDoneReservations) {
        this.countDoneReservations = countDoneReservations;
    }

    public Integer getCountLabDevices() {
        if(countLabDevices == null) calculateCounts();
        return countLabDevices;
    }

    public void setCountLabDevices(Integer countLabDevices) {
        this.countLabDevices = countLabDevices;
    }

    public Integer getCountLabDeviceGroups() {
        if(countLabDeviceGroups == null) calculateCounts();
        return countLabDeviceGroups;
    }

    public void setCountLabDeviceGroups(Integer countLabDeviceGroups) {
        this.countLabDeviceGroups = countLabDeviceGroups;
    }

    public Integer getCountUsers() {
        if(countUsers == null) calculateCounts();
        return countUsers;
    }

    public void setCountUsers(Integer countUsers) {
        this.countUsers = countUsers;
    }

    public Integer getProzRunReservation() {
        if(prozRunReservation == null) calculateCounts();
        return prozRunReservation;
    }

    public void setProzRunReservation(Integer prozRunReservation) {
        this.prozRunReservation = prozRunReservation;
    }

    public Integer getProzOpenReservations() {
        if(prozOpenReservations == null) calculateCounts();
        return prozOpenReservations;
    }

    public void setProzOpenReservations(Integer prozOpenReservations) {
        this.prozOpenReservations = prozOpenReservations;
    }

    public Integer getProzDueReservations() {
        if(prozDueReservations == null) calculateCounts();
        return prozDueReservations;
    }

    public void setProzDueReservations(Integer prozDueReservations) {
        this.prozDueReservations = prozDueReservations;
    }

    public Integer getProzDoneReservations() {
        if(prozDoneReservations == null) calculateCounts();
        return prozDoneReservations;
    }

    public void setProzDoneReservations(Integer prozDoneReservations) {
        this.prozDoneReservations = prozDoneReservations;
    }
}
