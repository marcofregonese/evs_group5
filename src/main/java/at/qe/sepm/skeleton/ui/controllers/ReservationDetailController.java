package at.qe.sepm.skeleton.ui.controllers;

import at.qe.sepm.skeleton.model.Reservation;
import at.qe.sepm.skeleton.services.ReservationService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.context.annotation.Scope;
import org.springframework.stereotype.Component;

/**
 * Created by ${Marc} on ${05.12.2018}.
 */

@Component
@Scope("view")
public class ReservationDetailController {

    @Autowired
    private ReservationService reservationService;

    private Reservation reservation;

    public void setReservation(Reservation reservation) {
        this.reservation = reservation;
        doReloadReservation();
    }

    public void doReloadReservation() {
        reservation = reservationService.getReservationById(reservation.getId());
    }

    public Reservation getReservation() {
        return this.reservation;
    }

    public void doSaveReservation() {
        reservation = this.reservationService.saveReservation(reservation);
    }

    public void doDeleteReservation() {
        this.reservationService.deleteReservation(reservation);
        reservation = null;
    }
}
