package at.qe.sepm.skeleton.ui.controllers;

import at.qe.sepm.skeleton.model.Setting;
import at.qe.sepm.skeleton.services.SettingService;
import java.io.Serializable;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.context.annotation.Scope;
import org.springframework.stereotype.Component;

/**
 *
 * @author Manuel
 */
@Component
@Scope("view")
public class SettingController implements Serializable{

    @Autowired
    private SettingService settingService;
    
    private Setting server;
    private Setting port;
    private Setting ssl;
    private Setting starttls;
    private Setting username;
    private Setting password;
    private Setting sendermail;
    
    public void loadSettings(){
        server = settingService.getBySettingName("MAIL:SERVER");
        port = settingService.getBySettingName("MAIL:PORT");
        ssl = settingService.getBySettingName("MAIL:SSL");
        starttls = settingService.getBySettingName("MAIL:STARTTLS");
        username = settingService.getBySettingName("MAIL:USERNAME");
        password = settingService.getBySettingName("MAIL:PASSWORD");
        sendermail = settingService.getBySettingName("MAIL:SENDER");
    }
    
    public void save(){
        settingService.save(server);
        settingService.save(port);
        settingService.save(ssl);
        settingService.save(starttls);
        settingService.save(username);
        settingService.save(password);
        settingService.save(sendermail);
    }

    public Setting getServer() {
        if(server == null) loadSettings();
        return server;
    }

    public void setServer(Setting server) {
        this.server = server;
    }

    public Setting getPort() {
        if(port == null) loadSettings();
        return port;
    }

    public void setPort(Setting port) {
        this.port = port;
    }

    public Setting getSsl() {
        if(ssl == null) loadSettings();
        return ssl;
    }

    public void setSsl(Setting ssl) {
        this.ssl = ssl;
    }

    public Setting getStarttls() {
        if(starttls == null) loadSettings();
        return starttls;
    }

    public void setStarttls(Setting starttls) {
        this.starttls = starttls;
    }

    public Setting getUsername() {
        if(username == null) loadSettings();
        return username;
    }

    public void setUsername(Setting username) {
        this.username = username;
    }

    public Setting getPassword() {
        if(password == null) loadSettings();
        return password;
    }

    public void setPassword(Setting password) {
        this.password = password;
    }

    public Setting getSendermail() {
        if(sendermail == null) loadSettings();
        return sendermail;
    }

    public void setSendermail(Setting sendermail) {
        this.sendermail = sendermail;
    }
}
