/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package at.qe.sepm.skeleton.ui.controllers;

import at.qe.sepm.skeleton.model.Holiday;
import at.qe.sepm.skeleton.services.HolidayService;
import java.util.Date;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.context.annotation.Scope;
import org.springframework.stereotype.Component;

/**
 *
 * @author Manuel
 */
@Component
@Scope("view")
public class HolidayDetailController {
 
    @Autowired
    HolidayService holidayService;
    
    private Holiday holiday;
    private Boolean addEnabled;
    
    public HolidayDetailController(){
        holiday = new Holiday();
        holiday.setDate(new Date());
        addEnabled = false;
    }
    
    public void inputChanged(){
        addEnabled = (holiday.getName() != null && holiday.getName().isEmpty() == false); 
    }
    
    public void save(){
        holidayService.saveHoliday(holiday);
        holiday = new Holiday();
        holiday.setDate(new Date());
        addEnabled = false;
    }
    
    public void delete(Holiday holiday){
        holidayService.deleteHoliday(holiday);
    }

    public Holiday getHoliday() {
        return holiday;
    }

    public void setHoliday(Holiday holiday) {
        this.holiday = holiday;
    }

    public Boolean getAddEnabled() {
        return addEnabled;
    }

    public void setAddEnabled(Boolean addEnabled) {
        this.addEnabled = addEnabled;
    }
}
