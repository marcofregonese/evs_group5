package at.qe.sepm.skeleton.ui.controllers;

import at.qe.sepm.skeleton.model.LabDevice;
import at.qe.sepm.skeleton.model.LabDeviceFile;
import at.qe.sepm.skeleton.model.Laboratory;
import at.qe.sepm.skeleton.model.Reservation;
import at.qe.sepm.skeleton.services.LabDeviceService;
import at.qe.sepm.skeleton.services.LaboratoryService;
import at.qe.sepm.skeleton.services.ReservationService;
import at.qe.sepm.skeleton.services.SchedulerService;
import org.primefaces.event.FileUploadEvent;
import org.primefaces.model.StreamedContent;
import org.primefaces.model.UploadedFile;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.context.annotation.Scope;
import org.springframework.stereotype.Component;

import javax.annotation.ManagedBean;
import javax.inject.Named;
import java.io.IOException;
import java.io.Serializable;
import java.time.LocalDate;
import java.time.LocalDateTime;
import java.time.LocalTime;
import java.time.ZoneId;
import java.util.ArrayList;
import java.util.Date;
import java.util.List;
import java.util.stream.Collectors;

/**
 * Created by ${Marc} on ${05.12.2018}.
 */
@Named
@ManagedBean
@Component
@Scope("application")
public class LabDeviceDetailController implements Serializable{

    @Autowired
    private LabDeviceService labDeviceService;
    
    @Autowired
    private LaboratoryService laboratoryService; 
    
    @Autowired
    private ReservationService reservationService;
    
    private List<String> laboratories;
    private LabDevice labDevice;
    private boolean addEnabled;
    private String laboratory;
    private Laboratory lab;

    private UploadedFile file;

    public LabDeviceDetailController(){
        labDevice = new LabDevice();
    }

    public void createNew(){
        this.laboratories = laboratoryService.getAllLaboratories().stream().map(Laboratory::getLaboratoryName).collect(Collectors.toList());
        labDevice = new LabDevice();
        addEnabled = false;
        laboratory = null;
    }
    
    public void setLabDevice(LabDevice labDevice) {
        this.labDevice = labDevice;
        this.laboratories = laboratoryService.getAllLaboratories().stream().map(Laboratory::getLaboratoryName).collect(Collectors.toList());
        inputChanged(false);
        doReloadLabDevice();
    }
    
    public void inputChanged(Boolean laboratory){
        if(laboratory) labDevice.setLaboratory(laboratoryService.getLaborytorybyName(this.laboratory));
        addEnabled = (labDevice.getDeviceModel() != null && !labDevice.getDeviceModel().isEmpty()
                && labDevice.getName() != null && !labDevice.getName().isEmpty()
                && labDevice.getLaboratory() != null && labDevice.getMaxDurationInHours() > 0);
    }

    public void doReloadLabDevice() {
        labDevice = labDeviceService.getLabDeviceById(labDevice.getId());
        if(labDevice.getLaboratory() != null) laboratory = labDevice.getLaboratory().getLaboratoryName();
        else laboratory = null;
    }

    public LabDevice getLabDevice() {
        return this.labDevice;
    }

    public void doSaveLabDevice() {
        labDevice = this.labDeviceService.saveLabDevice(labDevice);
    }

    public void doDeleteLabDevice() {
        this.labDeviceService.deleteLabDevice(labDevice);
        labDevice = null;
    }

    public void deleteFile(LabDeviceFile labDeviceFile){
        labDeviceService.deleteFile(labDeviceFile);
    }
    
    public StreamedContent downloadFile(String path, String fileName) throws IOException {
        return labDeviceService.downloadFile(path, fileName);
    }

    public void handleFileUpload(FileUploadEvent event) throws Exception {
        file = event.getFile();
        labDeviceService.upFile(file, labDevice);
    }


    public boolean isAddEnabled() {
        return addEnabled;
    }

    public void setAddEnabled(boolean addEnabled) {
        this.addEnabled = addEnabled;
    }

    public List<String> getLaboratories() {
        return laboratories;
    }

    public void setLaboratories(List<String> laboratories) {
        this.laboratories = laboratories;
    }

    public String getLaboratory() {
        return laboratory;
    }

    public void setLaboratory(String laboratory) {
        this.laboratory = laboratory;
    }

    public UploadedFile getFile() {
        return file;
    }

    public void setFile(UploadedFile file) {
        this.file = file;
    }
    
    public List<Reservation> getReservations(){
        return reservationService.getAllActiveReservationsFromLabDevice(labDevice.getId());
    }
    
    public Date convertToDate(LocalDate localDate, LocalTime localTime){
        return Date.from(LocalDateTime.of(localDate, localTime).atZone(ZoneId.of("CET")).toInstant());
    }
    
    public List<LabDevice> getAllLabDevices(){
    	if (lab != null) return labDeviceService.getAllLabDevicesOfLab(lab.getId());
    	else return new ArrayList<>();
    }

    public Laboratory getLab() {
        return lab;
    }

    public void setLab(Laboratory lab) {
        this.lab = lab;
        doReloadLaboratory();
    }
    
    private void doReloadLaboratory(){
        lab = laboratoryService.getLaboratoryById(lab.getId());
    }
    
    public String getFileCount(LabDevice labDevice) {
    	if(labDevice.getFiles() == null) return "0";
    	else return String.valueOf(labDevice.getFiles().size());
    }
}
