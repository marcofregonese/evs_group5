package at.qe.sepm.skeleton.ui.controllers;

import at.qe.sepm.skeleton.model.LabDevice;
import at.qe.sepm.skeleton.model.LabDeviceGroup;
import at.qe.sepm.skeleton.services.LabDeviceGroupService;
import at.qe.sepm.skeleton.ui.beans.SessionInfoBean;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.context.annotation.Scope;
import org.springframework.stereotype.Component;

import java.io.Serializable;
import java.util.List;

/**
 * Created by ${Marc} on ${21.10.2016}.
 */
@Component
@Scope("view")
public class LabDeviceGroupListController implements Serializable {

    @Autowired
    private LabDeviceGroupService labDeviceGroupService;
    
    @Autowired
    private SessionInfoBean sessionInfoBean;

    public List<LabDeviceGroup> getLabDeviceGroups() {
        if(sessionInfoBean.hasRole("ADMIN")) return labDeviceGroupService.getAllLabDeviceGroups();
        else return labDeviceGroupService.getLabDeviceGroupsOfUser(sessionInfoBean.getCurrentUser());
    }
    
    public void deleteLabDeviceGroup(LabDeviceGroup labDeviceGroup){
        labDeviceGroupService.deleteLabDeviceGroup(labDeviceGroup);
    }
    
    public boolean getState(LabDeviceGroup labDeviceGroup){
        for(LabDevice labDevice : labDeviceGroup.getLabDevices()){
            if(labDevice.isState() == false) return false;
        }
        return true;
    }
}
