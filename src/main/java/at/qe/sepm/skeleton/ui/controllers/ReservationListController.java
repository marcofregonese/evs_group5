package at.qe.sepm.skeleton.ui.controllers;

import at.qe.sepm.skeleton.model.Reservation;
import at.qe.sepm.skeleton.services.ReservationService;
import at.qe.sepm.skeleton.services.SchedulerService;
import at.qe.sepm.skeleton.ui.beans.SessionInfoBean;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.context.annotation.Scope;
import org.springframework.stereotype.Component;

import java.io.Serializable;
import java.time.LocalDateTime;
import java.time.ZoneId;
import java.util.Date;
import java.util.List;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

/**
 * Created by ${Marc} on ${05.12.2018}.
 */

@Component
@Scope("view")
public class ReservationListController implements Serializable{

    @Autowired
    private ReservationService reservationService;
    
    @Autowired
    private SessionInfoBean sessionInfoBean;


    public List<Reservation> getReservations() {
        if(sessionInfoBean.hasRole("ADMIN")) return reservationService.getAllReservations();
        else return reservationService.getAllReservationsFromUsername(sessionInfoBean.getCurrentUserName());
        
    }

    public List<Reservation> getAllReservationsFromUsername(String username) {
        return reservationService.getAllReservationsFromUsername(username);
    }

    public int getReservationCount() {
        return reservationService.getReservationCount();
    }
    
    public void delete(Reservation reservation){
        reservationService.deleteReservation(reservation);
    }

    public boolean isReservationInStatus(Reservation reservation, String status){
        if(status.equals("DUE")){
            Date resEndDate = Date.from(LocalDateTime.of(reservation.getEndDate(), reservation.getEndTime()).atZone(ZoneId.of("CET")).toInstant());
             return (reservation.getRestitutionDate() == null && resEndDate.before(new Date()));
        }
        else if(status.equals("OPEN")){
            Date resStartDate = Date.from(LocalDateTime.of(reservation.getStartDate(), reservation.getStartTime()).atZone(ZoneId.of("CET")).toInstant());
            return resStartDate.after(new Date());
        }
        else if(status.equals("RUN")){
            Date resStartDate = Date.from(LocalDateTime.of(reservation.getStartDate(), reservation.getStartTime()).atZone(ZoneId.of("CET")).toInstant());
            Date resEndDate = Date.from(LocalDateTime.of(reservation.getEndDate(), reservation.getEndTime()).atZone(ZoneId.of("CET")).toInstant());
            Date aktDate = new Date();
            return (resStartDate.before(aktDate) && resEndDate.after(aktDate) && reservation.getRestitutionDate() == null);
        }
        else return false;
    }
}
