package at.qe.sepm.skeleton.ui.controllers;

import at.qe.sepm.skeleton.model.LabDevice;
import at.qe.sepm.skeleton.model.LabDeviceGroup;
import at.qe.sepm.skeleton.model.Reservation;
import at.qe.sepm.skeleton.services.LabDeviceGroupService;
import at.qe.sepm.skeleton.services.LabDeviceService;
import at.qe.sepm.skeleton.services.ReservationService;
import at.qe.sepm.skeleton.services.SchedulerService;
import at.qe.sepm.skeleton.ui.beans.SessionInfoBean;
import org.primefaces.event.SelectEvent;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.context.annotation.Scope;
import org.springframework.stereotype.Component;

import javax.faces.application.FacesMessage;
import javax.faces.context.FacesContext;
import java.io.Serializable;
import java.util.ArrayList;
import java.util.List;
import java.util.stream.Collectors;

/**
 *
 * @author Manuel
 */
@Component
@Scope("session")
public class LabDeviceGroupDetailController implements Serializable {

    @Autowired
    private LabDeviceGroupService labDeviceGroupService;
    
    @Autowired
    private LabDeviceService labDeviceService;
    
    @Autowired
    private SessionInfoBean sessionInfoBean;
    
    @Autowired
    private ReservationService reservationService;
    
    private LabDeviceGroup labDeviceGroup;
    private List<LabDevice> labDevices;
    private Boolean addEnabled;
    private Boolean exists;
    
    public LabDeviceGroupDetailController(){
        labDeviceGroup = new LabDeviceGroup();
        addEnabled = false;
    }
    
    public void createNewLabDeviceGroup(){
        labDeviceGroup = new LabDeviceGroup();
        labDeviceGroup.setUserTag(sessionInfoBean.getCurrentUser());
        labDeviceGroup.setLabDevices(new ArrayList<>());
        labDevices = labDeviceService.getAllLabDevices();
        addEnabled = false;
    }
    
    public void inputChanged(boolean name){
        if(name == true && labDeviceGroup.getName() != null){ 
            LabDeviceGroup l = labDeviceGroupService.getLabDeviceGroupByName(labDeviceGroup.getName());
            if(l != null) {
                exists = true;
                FacesContext.getCurrentInstance().addMessage("name", new FacesMessage(FacesMessage.SEVERITY_ERROR, "Fehler!", "Dieser Gruppenname exestiert bereits!"));
            }
            else exists = false;
        }
        addEnabled = (labDeviceGroup.getName() != null && labDeviceGroup.getName().isEmpty() == false
                && labDeviceGroup.getLabDevices() != null && labDeviceGroup.getLabDevices().size() >= 2);
    }
    
    public void save(){
        labDeviceGroupService.saveLabDeviceGroup(labDeviceGroup);
        labDeviceGroup = new LabDeviceGroup();
        addEnabled = false;
    }

    public LabDeviceGroup getLabDeviceGroup() {
        return labDeviceGroup;
    }

    public void setLabDeviceGroup(LabDeviceGroup labDeviceGroup) {
        this.labDeviceGroup = labDeviceGroup;
        //doReloadLabDeviceGroup();
        labDevices = labDeviceService.getAllLabDevices();
        inputChanged(false);
    }
    
    public void doReloadLabDeviceGroup() {
        labDeviceGroup = labDeviceGroupService.getLabDeviceGroupById(labDeviceGroup.getId());
    }

    public Boolean getAddEnabled() {
        return addEnabled;
    }

    public void setAddEnabled(Boolean addEnabled) {
        this.addEnabled = addEnabled;
    }

    public List<LabDevice> getLabDevices() {
        return labDevices;
    }

    public void setLabDevices(List<LabDevice> labDevices) {
        this.labDevices = labDevices;
    }
    
    public List<LabDevice> completeDevices(String query) {
        List<LabDevice> labDevices = this.labDevices.stream().filter(x->x.getName().toLowerCase().startsWith(query.trim().toLowerCase())).collect(Collectors.toList());
        labDeviceGroup.getLabDevices().forEach(x -> {
            LabDevice labDevice = labDevices.stream().filter(y->y.getId() == x.getId()).findFirst().orElse(null);
            if(labDevice != null) labDevices.remove(labDevice);
        });
        return labDevices;
    }
    
    public void addDevice(SelectEvent event){
        LabDevice labDevice = labDevices.stream().filter(x->x.getId() == Integer.valueOf((String)event.getObject())).findFirst().orElse(null);
        labDeviceGroup.getLabDevices().add(labDevice);
        inputChanged(false);
    }
    
    public void deleteDevice(LabDevice labDevice){
        labDeviceGroup.getLabDevices().remove(labDevice);
        inputChanged(false);
    }
    
    public List<Reservation> getReservations(){
        if(labDeviceGroup.getId() == null) return new ArrayList<>();
        else {
            return reservationService.getAllActiveReservationsByLabDeviceGroupId(labDeviceGroup.getId());
        }
    }
}
