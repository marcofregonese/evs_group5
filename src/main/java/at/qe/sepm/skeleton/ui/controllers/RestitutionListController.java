/**
 * 
 */
package at.qe.sepm.skeleton.ui.controllers;

import java.io.Serializable;
import java.util.List;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.context.annotation.Scope;
import org.springframework.stereotype.Component;

import at.qe.sepm.skeleton.model.Reservation;
import at.qe.sepm.skeleton.model.UserRole;
import at.qe.sepm.skeleton.services.ReservationService;
import at.qe.sepm.skeleton.services.RestitutionService;
import at.qe.sepm.skeleton.ui.beans.SessionInfoBean;
import java.time.LocalDateTime;
import java.time.ZoneId;
import java.util.Date;

/**
 * @author marco
 *
 */

@Component
@Scope("view")
public class RestitutionListController implements Serializable{

    @Autowired
    private RestitutionService restitutionService;
    
    @Autowired
    private ReservationService reservationService;
    
    @Autowired
    private SessionInfoBean sessionInfoBean;
    
    public List<Reservation> getAllRestitutions(){
        if(sessionInfoBean.getCurrentUser().getRoles().contains(UserRole.ADMIN)) return restitutionService.getAllActiveRestitutions();
        else return restitutionService.getAllActiveRestitutionsFromUsername(sessionInfoBean.getCurrentUserName());
    }
    
    public List<Reservation> getAllRestitutionsFromUsername(String username){
    	return restitutionService.getAllRestitutionsFromUsername(username);
    }
    
    public List<Reservation> getAllActiveReservations(){
		return reservationService.getAllActiveReservations();
    }
    
    public List<Reservation> getAllActiveReservationsFromUsername(String username){
    	return reservationService.getAllActiveReservationsFromUsername(username);
    }

    public boolean isRestitutionInStatus(Reservation reservation, String status){
        if(status.equals("DUE")){
            Date resEndDate = Date.from(LocalDateTime.of(reservation.getEndDate(), reservation.getEndTime()).atZone(ZoneId.of("CET")).toInstant());
            return (reservation.getRestitutionDate() == null && resEndDate.before(new Date()));
        }
        else if(status.equals("RUN")){
            Date resStartDate = Date.from(LocalDateTime.of(reservation.getStartDate(), reservation.getStartTime()).atZone(ZoneId.of("CET")).toInstant());
            Date resEndDate = Date.from(LocalDateTime.of(reservation.getEndDate(), reservation.getEndTime()).atZone(ZoneId.of("CET")).toInstant());
            Date aktDate = new Date();
            return (resStartDate.before(aktDate) && resEndDate.after(aktDate) && reservation.getRestitutionDate() == null);
        }
        else if(status.equals("DONE")){
            return reservation.getRestitutionDate() != null;
        }
        else return false;
    }
}
