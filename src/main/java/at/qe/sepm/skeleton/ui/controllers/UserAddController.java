/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package at.qe.sepm.skeleton.ui.controllers;

import at.qe.sepm.skeleton.model.User;
import at.qe.sepm.skeleton.model.UserRole;
import at.qe.sepm.skeleton.services.UserService;
import at.qe.sepm.skeleton.ui.beans.SessionInfoBean;
import javax.faces.application.FacesMessage;
import javax.faces.context.FacesContext;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.context.annotation.Scope;
import org.springframework.stereotype.Component;

import java.io.Serializable;
import java.util.HashSet;

/**
 *
 * @author Manuel
 */
@Component
@Scope("view")
public class UserAddController implements Serializable {
    
    @Autowired
    private UserService userService;
    
    @Autowired
    private SessionInfoBean sessionInfoBean;
    
    private User user;
    private Boolean addEnabled;
    private Boolean roleAdmin;
    private Boolean roleEmployee;
    private Boolean exists;

    public UserAddController() {
        user = new User();
        addEnabled = false;
        roleAdmin = false;
        roleEmployee = false;
        exists = false;
    }
    
    public void inputChanged(boolean cident){
         if(cident == true && user.getcIdent() != null){ 
            user.setUsername(user.getcIdent());
            User u = userService.loadUser(user.getUsername());
            if(u != null) {
                exists = true;
                FacesContext.getCurrentInstance().addMessage("cident", new FacesMessage(FacesMessage.SEVERITY_ERROR, "Fehler!", "Diese C-Kennung exestiert bereits!"));
            }
            else exists = false;
        }
        
        addEnabled = (user.getEmail() != null && user.getEmail().isEmpty() == false 
            && user.getFirstName() != null && user.getFirstName().isEmpty() == false
            && user.getLastName() != null && user.getLastName().isEmpty() == false 
            && user.getPassword() != null && user.getPassword().isEmpty() == false
            && user.getcIdent() != null && user.getcIdent().isEmpty() == false
            && exists == false);
    }
    
    public void addUser(){
        user.setRoles(new HashSet<>());
        user.getRoles().add(UserRole.STUDENT);
        if(roleAdmin == true) user.getRoles().add(UserRole.ADMIN);
        if(roleEmployee == true) user.getRoles().add(UserRole.EMPLOYEE);
        userService.saveUser(user);
        
        user = new User();
        addEnabled = false;
        roleAdmin = false;
        roleEmployee = false;
        exists = false;
    }

    public User getUser() {
        return user;
    }

    public void setUser(User user) {
        this.user = user;
    }

    public Boolean getAddEnabled() {
        return addEnabled;
    }

    public void setAddEnabled(Boolean addEnabled) {
        this.addEnabled = addEnabled;
    }

    public Boolean getRoleAdmin() {
        return roleAdmin;
    }

    public void setRoleAdmin(Boolean roleAdmin) {
        this.roleAdmin = roleAdmin;
    }

    public Boolean getRoleEmployee() {
        return roleEmployee;
    }

    public void setRoleEmployee(Boolean roleEmployee) {
        this.roleEmployee = roleEmployee;
    }
}
