package at.qe.sepm.skeleton.repositories;

import at.qe.sepm.skeleton.model.LabDevice;
import at.qe.sepm.skeleton.model.LabDeviceFile;
import org.springframework.data.jpa.repository.Query;
import org.springframework.data.repository.query.Param;

import java.util.List;

/**
 * Created by ${Marc} on ${05.12.2018}.
 */
public interface LabDeviceFileRepository extends AbstractRepository<LabDeviceFile, Integer> {
    
    @Query("SELECT l FROM LabDeviceFile l WHERE l.path = :path AND l.labDeviceTag = :labDevice")
    LabDeviceFile getLabDeviceFileByNameAndLabDevice(@Param("path") String Path, @Param("labDevice") LabDevice labDevice);
}
