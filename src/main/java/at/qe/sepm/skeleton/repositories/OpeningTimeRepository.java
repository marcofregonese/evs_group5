/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package at.qe.sepm.skeleton.repositories;

import at.qe.sepm.skeleton.model.Laboratory;
import at.qe.sepm.skeleton.model.OpeningTime;
import java.util.List;
import org.springframework.data.jpa.repository.Query;
import org.springframework.data.repository.query.Param;

/**
 *
 * @author Manuel
 */
public interface OpeningTimeRepository extends AbstractRepository<OpeningTime, Integer> {
    
    OpeningTime findFirstById(int id);
    
    @Query("SELECT o FROM OpeningTime o WHERE o.laboratoryTag = :laboratoryTag")
    List<OpeningTime>findAllForLaboraty(@Param("laboratoryTag") Laboratory laboratory);
    
    @Query("SELECT o FROM OpeningTime o WHERE o.laboratoryTag = :laboratoryTag AND o.dayOfWeek = :dayOfWeek")
    OpeningTime findDayForLaboraty(@Param("laboratoryTag") Laboratory laboratory, @Param("dayOfWeek") String dayOfWeek);
}
