package at.qe.sepm.skeleton.repositories;

import at.qe.sepm.skeleton.model.LabDevice;
import org.springframework.data.jpa.repository.Query;
import org.springframework.data.repository.query.Param;

import java.util.List;

/**
 * Created by ${Marc} on ${05.12.2018}.
 */
public interface LabDeviceRepository extends AbstractRepository<LabDevice, Integer> {

    LabDevice findFirstById(int id);

    LabDevice findFirstByDeviceModel(String deviceModel);

    @Query("SELECT l FROM LabDevice l WHERE l.name = :name")
    List<LabDevice>findAllByName(@Param("name") String name);

    @Query("SELECT l FROM LabDevice l WHERE l.location = :location")
    List<LabDevice>findAllByLocation(@Param("location") String location);

    @Query("SELECT l.id FROM LabDevice l")
    List<Integer> getAllLabDeviceIds();

    @Query("SELECT l FROM LabDevice l WHERE l.name = :name")
    LabDevice getLabDeviceByName(@Param("name") String name);

    @Query("SELECT l FROM LabDevice l WHERE l.laboratoryTag.id = :id")
    List<LabDevice> getAllLabDevicesOfLab(@Param("id") Integer id);

}
