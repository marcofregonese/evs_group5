package at.qe.sepm.skeleton.repositories;

import at.qe.sepm.skeleton.model.LabDevice;
import at.qe.sepm.skeleton.model.Reservation;
import org.springframework.data.jpa.repository.Query;
import org.springframework.data.repository.query.Param;

import java.time.LocalDate;
import java.time.LocalTime;
import java.util.List;

/**
 * Created by ${Marc} on ${05.12.2018}.
 */
public interface ReservationRepository extends AbstractRepository<Reservation, Long> {

    @Query("SELECT r FROM Reservation r WHERE r.userTag.username = :username AND r.restitutionDate IS NULL")
    List<Reservation> getAllReservationsFromUsername(@Param("username") String username);
    
    @Query("SELECT r FROM Reservation r WHERE r.restitutionDate IS NULL")
    List<Reservation> getAllReservations();
    
    @Query("SELECT r FROM Reservation r WHERE r.userTag.username = :username")
    List<Reservation> getReservationsFromUsername(@Param("username") String username);
    
    @Query("SELECT r FROM Reservation r WHERE r.labDeviceTag.id = :labDeviceId AND r.restitutionDate IS NULL")
    List<Reservation> getAllActiveReservationFromLabDeviceId(@Param("labDeviceId") Integer labDeviceId);
    
    @Query("SELECT r FROM Reservation r WHERE r.labDeviceGroup.id = :labDeviceGroupId AND r.restitutionDate IS NULL")
    List<Reservation> getAllActiveReservationIdsByLabDeviceGroupId(@Param("labDeviceGroupId") Integer labDeviceGroupId);
    
    Reservation findFirstById(long id);
    
    @Query("SELECT r FROM Reservation r WHERE r.restitutionDate IS NULL")
    List<Reservation> getAllActiveReservations();
    
    @Query("SELECT r FROM Reservation r WHERE r.restitutionDate IS NOT NULL")
    List<Reservation> getAllRestitutions();
    
    @Query("SELECT r FROM Reservation r WHERE r.userTag.username = :username AND r.restitutionDate IS NULL")
    List<Reservation> getAllActiveReservationsFromUsername(@Param("username") String username);
    
    @Query("SELECT r FROM Reservation r WHERE r.userTag.username = :username AND r.restitutionDate IS NOT NULL")
    List<Reservation> getAllRestitutionsFromUsername(@Param("username") String username);

    @Query("SELECT r.labDeviceTag FROM Reservation r WHERE r.labDeviceTag.id = :id")
    List<LabDevice> getAllReservationsFromLabDeviceId(@Param("id") Integer id);
    
    @Query("SELECT r FROM Reservation r WHERE r.startDate < :aktDate AND r.startTime < :aktTime")
    List<Reservation> getAllActiveRestitutions(@Param("aktDate") LocalDate aktDate, @Param("aktTime") LocalTime aktTime);

    @Query("SELECT r FROM Reservation r WHERE r.startDate < :aktDate AND r.startTime < :aktTime AND r.userTag.username = :username")
    List<Reservation> getAllActiveRestitutionsFromUsername(@Param("username")String username, @Param("aktDate") LocalDate aktDate, @Param("aktTime") LocalTime aktTime);
    
    @Query("SELECT r FROM Reservation r WHERE ((r.startDate < :date AND r.endDate > :date)" + 
            " OR (r.endDate = :date AND r.endTime >= :time) OR (r.startDate = :date AND r.startTime <= :time )) AND r.labDeviceTag.id = :lid")
    List<Reservation> getAllDisruptedReservationsByDate(@Param("date") LocalDate date, @Param("time") LocalTime time, @Param("lid") Integer labDeviceId);
    
    @Query("SELECT r FROM Reservation r WHERE ((r.startDate > :startDate AND r.endDate < :endDate) OR " + 
            "(r.startDate = :startDate AND r.startTime > :startTime AND r.endDate = :endDate AND r.endTime < :endTime) OR " + 
            "(r.startDate = :startDate AND r.startDate > :startTime AND r.endDate < :endDate) OR " + 
            "(r.startDate > :startDate AND r.endDate = :endDate AND r.endTime < :endTime)) AND r.labDeviceTag.id = :labDeviceId")
    List<Reservation> getAllDiruptedReservationsByDateSpan(@Param("startDate") LocalDate startDate, 
                                                            @Param("startTime") LocalTime startTime, 
                                                            @Param("endDate") LocalDate endDate, 
                                                            @Param("endTime") LocalTime endTime,
                                                            @Param("labDeviceId") Integer labDeviceId);
    
    @Query("SELECT l FROM LabDevice l, Reservation m JOIN l.reservations r WHERE " +
            "(l.id = m.labDeviceTag.id) " +
            "AND l.deviceModel = :labDeviceModel " +
            "AND (NOT ((:startDate BETWEEN r.startDate AND r.endDate) AND r.endDate = :startDate AND (r.endTime BETWEEN :morning AND :startTime)))" +
            "AND (NOT ((:startDate BETWEEN r.startDate AND r.endDate) AND r.startDate = :endDate AND (r.startTime BETWEEN :endTime AND :midnight)))" +
            "AND (NOT ((:endDate BETWEEN r.startDate AND r.endDate) AND r.endDate = :startDate AND (r.endTime BETWEEN :morning AND :startTime)))" +
            "AND (NOT ((:endDate BETWEEN r.startDate AND r.endDate) AND r.startDate = :endDate AND (r.startTime BETWEEN :endTime AND :midnight)))" +
            "AND (NOT ((:startDate BETWEEN r.startDate AND r.endDate) AND (:endDate BETWEEN r.startDate AND r.endDate)))")
    List<LabDevice> getReservableLabDevicesOfModel(@Param("startDate") LocalDate startDate,
                                            @Param("endDate") LocalDate endDate, @Param("startTime") LocalTime startTime, @Param("endTime") LocalTime endTime,
                                            @Param("morning") LocalTime morning, @Param("midnight") LocalTime midnight, @Param("labDeviceModel") String labDeviceModel);

    @Query("SELECT r FROM Reservation r WHERE (r.remindMail = NULL OR r.remindMail = FALSE) AND r.startTime = :startTime AND r.startDate = :startDate " + 
            " AND ((r.endDate = :endDate AND r.endTime >= :endTime) OR r.endDate > :endDate)")
    List<Reservation> getAllReservationsForReminderMail(@Param("startTime") LocalTime startTime, @Param("startDate") LocalDate startDate,
            @Param("endTime") LocalTime endTime, @Param("endDate") LocalDate endDate);
    
    @Query("SELECT r FROM Reservation r WHERE r.startTime = :startTime AND r.startDate = :startDate " + 
            " AND r.endDate = :endDate AND r.endTime = :endTime AND r.userTag.username = :username AND r.labDeviceGroup.id = :labDeviceGroupId")
    List<Reservation> getAllReservationsFromGroupReservation(@Param("startTime") LocalTime startTime, @Param("startDate") LocalDate startDate,
            @Param("endTime") LocalTime endTime, @Param("endDate") LocalDate endDate, @Param("username") String username, @Param("labDeviceGroupId") Integer labDeviceGroupId);
    
    @Query("SELECT r FROM Reservation r WHERE ((r.endDate = :endDate AND r.endTime < :endTime) OR r.endDate < :endDate) AND r.restitutionDate IS NULL AND (r.overdueMail IS NULL OR r.overdueMail = FALSE)")
    List<Reservation> getAllOverdueReservationsFromDate(@Param("endDate") LocalDate endDate, @Param("endTime") LocalTime endTime);
}
