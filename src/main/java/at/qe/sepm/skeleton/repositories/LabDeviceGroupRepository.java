package at.qe.sepm.skeleton.repositories;

import at.qe.sepm.skeleton.model.LabDeviceGroup;
import org.springframework.data.jpa.repository.Query;
import org.springframework.data.repository.query.Param;

import java.util.List;

/**
 * Created by ${Marc} on ${21.10.2016}.
 */
public interface LabDeviceGroupRepository extends AbstractRepository<LabDeviceGroup, Integer> {

    @Query("SELECT l FROM LabDeviceGroup l WHERE l.id = :id")
    List<String> getDeviceModelsOfGroup(@Param("id") Integer id);

    @Query("SELECT l FROM LabDeviceGroup l WHERE l.userTag.id = :username")
    List<LabDeviceGroup> getLabDeviceGroupsOfUser(@Param("username") String username);
    
    @Query("SELECT l FROM LabDeviceGroup l WHERE l.name = :name")
    LabDeviceGroup getLabDeviceGroupByName(@Param("name") String name);

}
