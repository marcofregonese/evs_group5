/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package at.qe.sepm.skeleton.repositories;

import at.qe.sepm.skeleton.model.Holiday;
import java.util.Date;
import java.util.List;
import org.springframework.data.jpa.repository.Query;

/**
 *
 * @author Manuel
 */
public interface HolidayRepository extends AbstractRepository<Holiday, Date> {
    
    @Override
    @Query("SELECT h FROM Holiday h ORDER BY h.date ASC")
    List<Holiday> findAll();
}
