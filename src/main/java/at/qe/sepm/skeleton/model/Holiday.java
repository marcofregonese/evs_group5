/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package at.qe.sepm.skeleton.model;

import org.springframework.data.domain.Persistable;

import javax.persistence.Entity;
import javax.persistence.Id;
import java.util.Date;

/**
 *
 * @author Manuel
 */
@Entity
public class Holiday implements Persistable<Date> {
    private static final long serialVersionUID = 1L;
    @Id
    private Date date;
    
    private String name;

    public Date getDate() {
        return date;
    }

    public void setDate(Date date) {
        this.date = date;
    }

    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name;
    }

    @Override
    public Date getId() {
        return date;
    }

    @Override
    public boolean isNew() {
        return (date == null);
    }


}
