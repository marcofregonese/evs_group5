package at.qe.sepm.skeleton.model;

import org.springframework.data.domain.Persistable;

import javax.persistence.*;
import java.time.LocalDate;
import java.time.LocalTime;
import java.util.Date;

/**
 * Created by ${Marc} on ${05.12.2018}.
 */

@Entity
public class Reservation implements Persistable<Integer> {

    private static final long serialVersionUID = 1L;
    @Id
    @GeneratedValue(strategy = GenerationType.AUTO)
    @Column(name = "res_id")
    private Integer id;
    @Column
    private LocalDate startDate;
    @Column
    private LocalDate endDate;
    @Column
    private LocalTime startTime;
    @Column
    private LocalTime endTime;
    @ManyToOne
    private User userTag;
    @ManyToOne
    private LabDevice labDeviceTag;

    private String annotation;

    @ManyToOne
    private LabDeviceGroup labDeviceGroup;

    @Column
    private String adminUsername;
    @Temporal(TemporalType.TIMESTAMP)
    private Date restitutionDate;
    
    private Boolean overdueMail;
    private Boolean remindMail;
   

    public String getAdminUsername() {
		return adminUsername;
	}

	public void setAdminUsername(String adminUsername) {
		this.adminUsername = adminUsername;
	}

	public Date getRestitutionDate() {
		return restitutionDate;
	}

	public void setRestitutionDate(Date restitutionDate) {
		this.restitutionDate = restitutionDate;
	}

	public LocalDate getStartDate() {
        return startDate;
    }

    public void setStartDate(LocalDate startDate) {
        this.startDate = startDate;
    }

    public LocalDate getEndDate() {
        return endDate;
    }

    public void setEndDate(LocalDate endDate) {
        this.endDate = endDate;
    }

    public LocalTime getStartTime() {
        return startTime;
    }

    public void setStartTime(LocalTime startTime) {
        this.startTime = startTime;
    }

    public LocalTime getEndTime() {
        return endTime;
    }

    public void setEndTime(LocalTime endTime) {
        this.endTime = endTime;
    }

    public User getUserTag() {
        return userTag;
    }

    public void setUserTag(User userId) {
        this.userTag = userId;
    }

    public LabDevice getLabDeviceTag() {
        return labDeviceTag;
    }

    public void setLabDeviceTag(LabDevice labDeviceTag) {
        this.labDeviceTag = labDeviceTag;
    }

    public Integer getId() {
        return id;
    }

    @Override
    public boolean isNew() {
        return false;
    }

    public void setId(Integer id) {
        this.id = id;
    }

    public LabDeviceGroup getLabDeviceGroup() {
        return labDeviceGroup;
    }

    public void setLabDeviceGroup(LabDeviceGroup labDeviceGroup) {
        this.labDeviceGroup = labDeviceGroup;
    }

    public String getAnnotation() {
        return annotation;
    }

    public void setAnnotation(String annotation) {
        this.annotation = annotation;
    }

    public Boolean getOverdueMail() {
        return overdueMail;
    }

    public void setOverdueMail(Boolean overdueMail) {
        this.overdueMail = overdueMail;
    }

    public Boolean getRemindMail() {
        return remindMail;
    }

    public void setRemindMail(Boolean remindMail) {
        this.remindMail = remindMail;
    }
}
