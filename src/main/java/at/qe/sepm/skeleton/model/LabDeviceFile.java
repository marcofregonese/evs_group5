/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package at.qe.sepm.skeleton.model;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.ManyToOne;
import org.springframework.data.domain.Persistable;

/**
 *
 * @author Manuel
 */
@Entity
public class LabDeviceFile implements Persistable<Integer> {
    private static final long serialVersionUID = 1L;
    @Id
    @GeneratedValue(strategy = GenerationType.AUTO)
    private Integer id;
    @ManyToOne(optional = false)
    private LabDevice labDeviceTag;
    @Column(nullable = false)
    private String path;
    @Column(nullable = false)
    private String fileName;
    
    public Integer getId() {
        return id;
    }

    public void setId(Integer id) {
        this.id = id;
    }

    public LabDevice getLabDeviceTag() {
        return labDeviceTag;
    }

    public void setLabDeviceTag(LabDevice labDeviceTag) {
        this.labDeviceTag = labDeviceTag;
    }

    public String getFileName() {
        return fileName;
    }

    public void setFileName(String fileName) {
        this.fileName = fileName;
    }

    public String getPath() {
        return path;
    }

    public void setPath(String path) {
        this.path = path;
    }
    
    
    @Override
    public boolean isNew() {
        return (id == null);
    }
}
