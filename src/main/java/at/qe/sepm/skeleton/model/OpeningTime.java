/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package at.qe.sepm.skeleton.model;

import org.springframework.data.domain.Persistable;

import javax.persistence.*;
import java.util.Date;

/**
 *
 * @author Manuel
 */
@Entity
public class OpeningTime implements Persistable<Integer> {
    private static final long serialVersionUID = 1L;
    @Id
    @GeneratedValue(strategy = GenerationType.AUTO)
    private Integer id;
    @ManyToOne(optional = false)
    private Laboratory laboratoryTag;
    @Column
    private Date openTime;
    @Column
    private Date closeTime;
    @Column(nullable = false, length = 3)
    private String dayOfWeek;

    public Integer getId() {
        return id;
    }

    public void setId(Integer id) {
        this.id = id;
    }

    public Laboratory getLaboratoryTag() {
        return laboratoryTag;
    }

    public void setLaboratoryTag(Laboratory laboratoryTag) {
        this.laboratoryTag = laboratoryTag;
    }

    public Date getOpenTime() {
        return openTime;
    }

    public void setOpenTime(Date openTime) {
        this.openTime = openTime;
    }

    public Date getCloseTime() {
        return closeTime;
    }

    public void setCloseTime(Date closeTime) {
        this.closeTime = closeTime;
    }

    public String getDayOfWeek() {
        return dayOfWeek;
    }

    public void setDayOfWeek(String dayOfWeek) {
        this.dayOfWeek = dayOfWeek;
    }
    
    
    @Override
    public boolean isNew() {
        return (id == null);
    }
}
