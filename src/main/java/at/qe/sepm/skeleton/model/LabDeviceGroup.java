package at.qe.sepm.skeleton.model;

import org.hibernate.annotations.LazyCollection;
import org.hibernate.annotations.LazyCollectionOption;
import org.springframework.data.domain.Persistable;

import javax.persistence.*;
import java.util.List;

/**
 * Created by ${Marc} on ${09.01.2019}.
 */
@Entity
public class LabDeviceGroup implements Persistable<Integer> {

    private static final long serialVersionUID = 1L;
    @Id
    @GeneratedValue(strategy = GenerationType.AUTO)
    private Integer id;

    @Column (nullable = false, unique = true)
    private String name;

    @ManyToMany(cascade = CascadeType.ALL)
    @LazyCollection(LazyCollectionOption.FALSE)
    private List<LabDevice> labDevices;

    @ManyToOne
    private User userTag;
    
    @OneToMany(orphanRemoval = true, cascade = {CascadeType.MERGE, CascadeType.REMOVE, CascadeType.REFRESH}, mappedBy = "labDeviceGroup")
    @LazyCollection(LazyCollectionOption.FALSE)
    private List<Reservation> reservations;

    public Integer getId() {
        return this.id;
    }

    @Override
    public boolean isNew() {
        return false;
    }

    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name;
    }

    public List<LabDevice> getLabDevices() {
        return labDevices;
    }

    public void setLabDevices(List<LabDevice> labDevices) {
        this.labDevices = labDevices;
    }

    public void setId(Integer id) {
        this.id = id;
    }

    public User getUserTag() {
        return userTag;
    }

    public void setUserTag(User userTag) {
        this.userTag = userTag;
    }

    public List<Reservation> getReservations() {
        return reservations;
    }

    public void setReservations(List<Reservation> reservations) {
        this.reservations = reservations;
    }
}
