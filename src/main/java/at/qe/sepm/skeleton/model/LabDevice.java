package at.qe.sepm.skeleton.model;

import org.hibernate.annotations.LazyCollection;
import org.hibernate.annotations.LazyCollectionOption;
import org.springframework.data.domain.Persistable;

import javax.persistence.*;
import java.util.List;

/**
 * Created by ${Marc} on ${05.12.2018}.
 */
@Entity
public class LabDevice implements Persistable<Integer> {


    private static final long serialVersionUID = 1L;
    @Id
    @GeneratedValue(strategy = GenerationType.AUTO)
    @Column(name = "device_id")
    private Integer id;

    @Column(nullable = false, unique = true)
    private String name;
    
    @ManyToOne(optional = true)
    private Laboratory laboratoryTag;
    @Column(nullable = false)
    private String deviceModel;
    @Column(nullable = false)
    private String location;
    private String annotation;
    @Column(nullable = false)
    private int maxDurationInHours;
    private boolean state;

    @Column
    @ManyToMany(mappedBy = "labDevices", cascade = CascadeType.ALL)
    @LazyCollection(LazyCollectionOption.FALSE)
    private List<LabDeviceGroup> labDeviceGroupTag;

    @OneToMany(orphanRemoval = true, cascade = {CascadeType.MERGE, CascadeType.REMOVE, CascadeType.REFRESH}, mappedBy = "labDeviceTag")
    @LazyCollection(LazyCollectionOption.FALSE)
    private List<Reservation> reservations;

    @OneToMany(orphanRemoval = true, cascade = {CascadeType.MERGE, CascadeType.REMOVE, CascadeType.REFRESH}, mappedBy = "labDeviceTag")
    @LazyCollection(LazyCollectionOption.FALSE)
    private List<LabDeviceFile> files;





    @Override
    public Integer getId() {
        return id;
    }

    @Override
    public boolean isNew() {
        return false;
    }

    public void setId(Integer id) {
        this.id = id;
    }

    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name;
    }

    public Laboratory getLaboratory() {
        return laboratoryTag;
    }

    public void setLaboratory(Laboratory laboratoryTag) {
        this.laboratoryTag = laboratoryTag;
    }

    public String getLocation() {
        return location;
    }

    public void setLocation(String location) {
        this.location = location;
    }

    public String getAnnotation() {
        return annotation;
    }

    public void setAnnotation(String annotation) {
        this.annotation = annotation;
    }

    public int getMaxDurationInHours() {
        return maxDurationInHours;
    }
    
    public void setMaxDurationInHours(int maxDurationInHours) {
        this.maxDurationInHours = maxDurationInHours;
    }

    public boolean isState() {
        return state;
    }

    public void setState(boolean state) {
        this.state = state;
    }

    public List<Reservation> getReservations() {
        return reservations;
    }

    public void setReservations(List<Reservation> reservations) {
        this.reservations = reservations;
    }

    public String getDeviceModel() {
        return deviceModel;
    }

    public void setDeviceModel(String deviceModel) {
        this.deviceModel = deviceModel;
    }

    public List<LabDeviceFile> getFiles() {
        return files;
    }

    public void setFiles(List<LabDeviceFile> files) {
        this.files = files;
    }


    public List<LabDeviceGroup> getLabDeviceGroupTag() {
        return labDeviceGroupTag;
    }

    public void setLabDeviceGroupTag(List<LabDeviceGroup> labDeviceGroupTag) {
        this.labDeviceGroupTag = labDeviceGroupTag;
    }
}
