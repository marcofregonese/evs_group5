/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package at.qe.sepm.skeleton.services;

import at.qe.sepm.skeleton.model.Reservation;
import at.qe.sepm.skeleton.model.User;
import java.text.DateFormat;
import java.text.SimpleDateFormat;
import java.time.LocalDateTime;
import java.time.ZoneId;
import java.util.Date;
import java.util.List;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.scheduling.annotation.Scheduled;
import org.springframework.stereotype.Component;


/**
 *
 * @author Manuel
 */
@Component
public class SchedulerService {
    
    @Autowired
    private MailService mailService;
    
    @Autowired
    private ReservationService reservationService;
    
    @Autowired UserService userService;
    
    private static final Logger LOGGER = LoggerFactory.getLogger(SchedulerService.class);

//   second, minute, hour, day of month, month, day(s) of week
//  (*) means match any
//  */X means "every X"
    
    @Scheduled(cron = "0 * * * * *")
    public void runCron() {       
        LOGGER.info("Cron Job running");
        sendReminderMails();
        sendOverdueMails();
    }  
    
    private void sendReminderMails(){
        List<Reservation> reservations = reservationService.getAllReservationsForReminderMail();
        for(Reservation reservation : reservations){
            String usermail = reservation.getUserTag().getEmail();
            if(usermail != null && usermail.isEmpty() == false){
                DateFormat dateFormat = new SimpleDateFormat("dd.MM.yyyy HH:mm");
                Date start = Date.from(LocalDateTime.of(reservation.getStartDate(), reservation.getStartTime()).atZone(ZoneId.of("CET")).toInstant());
                Date end = Date.from(LocalDateTime.of(reservation.getEndDate(), reservation.getEndTime()).atZone(ZoneId.of("CET")).toInstant());
                if(reservation.getLabDeviceGroup() == null){
                    String mailtext = "Erinnerung an Ihre Gerätreservierung\n\n";
                    mailtext += "Start Ihrer Reservierung: " + dateFormat.format(start) + "\n";
                    mailtext += "Ende Ihrer Reservierung: " + dateFormat.format(end) + "\n\n";
                    mailtext += "Ihre Reservierung beinhaltet folgendes Gerät (Labor - Standort):\n";
                    mailtext += "\t"+ "- " + reservation.getLabDeviceTag().getName() + " (" + reservation.getLabDeviceTag().getLaboratory().getLaboratoryName() + " - " + reservation.getLabDeviceTag().getLocation() + ")\n";
                    mailtext += "\nMit freundlichen Grüßen\nUIBK Geräte Team";
                    mailService.SendMail(usermail, "UIBK Geräte: Erinnerung an Ihre Gerätreservierung", mailtext);
                    reservation.setRemindMail(Boolean.TRUE);
                    reservationService.saveReservation(reservation);
                }
                else{ 
                    String mailtext = "Erinnerung an Ihre Gruppenreservierung\n\n";
                    mailtext += "Start Ihrer Reservierung: " + dateFormat.format(start) + "\n";
                    mailtext += "Ende Ihrer Reservierung: " + dateFormat.format(end) + "\n\n";
                    mailtext += "Ihre Reservierung beinhaltet die Gerätegruppe " + reservation.getLabDeviceGroup().getName() + " mit folgenden Geräten (Labor - Standort):\n";
                    List<Reservation> respos = reservationService.getAllReservationsFromGroupReservation(reservation.getStartDate(), reservation.getStartTime(), reservation.getEndDate(), reservation.getEndTime(), reservation.getUserTag().getUsername(), reservation.getLabDeviceGroup().getId());
                    for(Reservation res : respos) {
                        mailtext += "\t" + "- " + res.getLabDeviceTag().getName() + " (" + res.getLabDeviceTag().getLaboratory().getLaboratoryName() + " - " + res.getLabDeviceTag().getLocation() + ")\n";
                        res.setRemindMail(Boolean.TRUE);
                        reservationService.saveReservation(res);
                    }
                    reservation.setRemindMail(Boolean.TRUE);
                    reservationService.saveReservation(reservation);
                    mailtext += "\nMit freundlichen Grüßen\nUIBK Geräte Team";
                    mailService.SendMail(usermail, "UIBK Geräte: Errinnerung an Ihre Gruppenreservierung", mailtext);
                }
            }
        }
    }
    
    private void sendOverdueMails(){
        List<Reservation> reservations = reservationService.getAllOverdueReservations();
        for(Reservation reservation : reservations){
            String usermail = reservation.getUserTag().getEmail();
            if(usermail != null && usermail.isEmpty() == false){
                DateFormat dateFormat = new SimpleDateFormat("dd.MM.yyyy HH:mm");
                Date end = Date.from(LocalDateTime.of(reservation.getEndDate(), reservation.getEndTime()).atZone(ZoneId.of("CET")).toInstant());
                if(reservation.getLabDeviceGroup() == null){
                    String mailtext = "Die Abgabe Ihrer Gerätreservierung ist überfällig\n\n";
                    mailtext += "Das Ende Ihrer Reservierung war: " + dateFormat.format(end) + "\n\n";
                    mailtext += "Ihre Reservierung beinhaltete folgendes Gerät (Labor - Standort):\n";
                    mailtext += "\t"+ "- " + reservation.getLabDeviceTag().getName() + " (" + reservation.getLabDeviceTag().getLaboratory().getLaboratoryName() + " - " + reservation.getLabDeviceTag().getLocation() + ")\n\n";
                    mailtext += "Bitte geben Sie das Gerät so schnell wie möglich zurück. Beachten Sie dazu auch die Öffnungszeiten des Labors.\n\n";
                    mailtext += "Mit freundlichen Grüßen\nUIBK Geräte Team";
                    mailService.SendMail(usermail, "UIBK Geräte: Abgabe Ihrer Gerätreservierung überfällig!", mailtext);
                    reservation.setOverdueMail(Boolean.TRUE);
                    reservationService.saveReservation(reservation);
                }
                else{ 
                    String mailtext = "Die Abgabe Ihrer Gruppenreservierung ist überfällig\n\n";
                    mailtext += "Das Ende Ihrer Reservierung war: " + dateFormat.format(end) + "\n\n";
                    mailtext += "Ihre Reservierung beinhaltete die Gerätegruppe " + reservation.getLabDeviceGroup().getName() + " mit folgenden Geräten (Labor - Standort):\n";
                    List<Reservation> respos = reservationService.getAllReservationsFromGroupReservation(reservation.getStartDate(), reservation.getStartTime(), reservation.getEndDate(), reservation.getEndTime(), reservation.getUserTag().getUsername(), reservation.getLabDeviceGroup().getId());
                    for(Reservation res : respos) {
                        mailtext += "\t" + "- " + res.getLabDeviceTag().getName() + " (" + res.getLabDeviceTag().getLaboratory().getLaboratoryName() + " - " + res.getLabDeviceTag().getLocation() + ")\n";
                        res.setOverdueMail(Boolean.TRUE);
                        reservationService.saveReservation(res);
                    }
                    reservation.setOverdueMail(Boolean.TRUE);
                    reservationService.saveReservation(reservation);
                    mailtext += "\nBitte geben Sie die Geräte so schnell wie möglich zurück. Beachten Sie dazu auch die Öffnungszeiten der Labore.\n\n";
                    mailtext += "Mit freundlichen Grüßen\nUIBK Geräte Team";
                    mailService.SendMail(usermail, "UIBK Geräte: Abgabe Ihrer Gruppenreservierung überfällig!", mailtext);
                }
                sendOverdueMailToAdmins(reservation);
            }
        }
    }
    
    private void sendOverdueMailToAdmins(Reservation reservation){
        List<User> admins = userService.getAllAdmins();
        for(User admin : admins){
            if(admin.getEmail() != null && admin.getEmail().isEmpty() == false){
                DateFormat dateFormat = new SimpleDateFormat("dd.MM.yyyy HH:mm");
                Date end = Date.from(LocalDateTime.of(reservation.getEndDate(), reservation.getEndTime()).atZone(ZoneId.of("CET")).toInstant());
                if(reservation.getLabDeviceGroup() == null){
                    String mailtext = "Die Abgabe einer Gerätreservierung ist überfällig\n\n";
                    mailtext += "Benutzer der Reservierung war: " + reservation.getUserTag().getcIdent() + " - " + reservation.getUserTag().getFirstName() + " " +reservation.getUserTag().getLastName() + "\n";
                    mailtext += "Das Ende der Reservierung war: " + dateFormat.format(end) + "\n\n";
                    mailtext += "Die Reservierung beinhaltete folgendes Gerät (Labor - Standort):\n";
                    mailtext += "\t"+ "- " + reservation.getLabDeviceTag().getName() + " (" + reservation.getLabDeviceTag().getLaboratory().getLaboratoryName() + " - " + reservation.getLabDeviceTag().getLocation() + ")\n\n";
                    mailtext += "Mit freundlichen Grüßen\nUIBK Geräte Team";
                    mailService.SendMail(admin.getEmail(), "UIBK Geräte: Abgabe einer Gerätreservierung überfällig!", mailtext);
                }
                else{ 
                    String mailtext = "Die Abgabe der Gerätreservierung ist überfällig\n\n";
                    mailtext += "Benutzer der Reservierung war: " + reservation.getUserTag().getcIdent() + " - " + reservation.getUserTag().getFirstName() + " " +reservation.getUserTag().getLastName() + "\n";
                    mailtext += "Das Ende der Reservierung war: " + dateFormat.format(end) + "\n\n";
                    mailtext += "Die Reservierung beinhaltete die Gerätegruppe " + reservation.getLabDeviceGroup().getName() + " mit folgenden Geräten (Labor - Standort):\n";
                    List<Reservation> respos = reservationService.getAllReservationsFromGroupReservation(reservation.getStartDate(), reservation.getStartTime(), reservation.getEndDate(), reservation.getEndTime(), reservation.getUserTag().getUsername(), reservation.getLabDeviceGroup().getId());
                    for(Reservation res : respos) {
                        mailtext += "\t" + "- " + res.getLabDeviceTag().getName() + " (" + res.getLabDeviceTag().getLaboratory().getLaboratoryName() + " - " + res.getLabDeviceTag().getLocation() + ")\n";
                    }
                    mailtext += "Mit freundlichen Grüßen\nUIBK Geräte Team";
                    mailService.SendMail(admin.getEmail(), "UIBK Geräte: Abgabe einer Gruppenreservierung überfällig!", mailtext);
                }
            }
        }
    }
}
