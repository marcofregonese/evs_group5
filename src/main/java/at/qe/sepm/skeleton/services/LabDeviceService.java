package at.qe.sepm.skeleton.services;

import at.qe.sepm.skeleton.model.LabDevice;
import at.qe.sepm.skeleton.model.LabDeviceFile;
import at.qe.sepm.skeleton.repositories.LabDeviceFileRepository;
import at.qe.sepm.skeleton.repositories.LabDeviceRepository;
import org.apache.commons.io.FilenameUtils;
import org.primefaces.model.DefaultStreamedContent;
import org.primefaces.model.StreamedContent;
import org.primefaces.model.UploadedFile;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.context.annotation.Scope;
import org.springframework.stereotype.Component;

import java.io.File;
import java.io.FileInputStream;
import java.io.FileNotFoundException;
import java.io.IOException;
import java.nio.file.Paths;
import java.util.LinkedList;
import java.util.List;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

/**
 * Created by ${Marc} on ${05.12.2018}.
 */

@Component
@Scope("application")
public class LabDeviceService {

    @Autowired
    LabDeviceRepository labDeviceRepository;
    
    @Autowired
    LabDeviceFileRepository labDeviceFileRepository;
    
    private static final Logger logger = LoggerFactory.getLogger(SchedulerService.class);
    
    public List<LabDevice> getAllLabDevices() {
        return labDeviceRepository.findAll();
    }

    public List<LabDevice> getLabDevicesAllByName(String name) {
        return labDeviceRepository.findAllByName(name);
    }

    public LabDevice getLabDeviceByName(String name) { 
        return labDeviceRepository.getLabDeviceByName(name); 
    }

    public LabDevice getLabDeviceById(int id) {
        return labDeviceRepository.findFirstById(id);
    }

    public List<LabDevice> getAllLabDevicesOfLab(Integer labId) {
        return labDeviceRepository.getAllLabDevicesOfLab(labId);
    }

    public List<LabDevice> getAllLabDevicesByLocation(String location) {
        return labDeviceRepository.findAllByLocation(location);
    }

    public LabDevice getFirstByDeviceModel(String deviceModel) {
        return labDeviceRepository.findFirstByDeviceModel(deviceModel);
    }

    public LabDevice saveLabDevice(LabDevice labDevice) {
        return labDeviceRepository.save(labDevice);
    }

    public void deleteLabDevice(LabDevice labDevice) {
        labDevice.getFiles().forEach(x->deleteFile(x));
        labDeviceRepository.delete(labDevice);
    }

    public List<Integer> getAllLabDeviceIds () {
        return labDeviceRepository.getAllLabDeviceIds();
    }
    
    public void deleteFile(LabDeviceFile labDeviceFile){
        File delFile = new File(labDeviceFile.getPath());
        if(delFile.exists()){ 
            if(delFile.delete()){
                labDeviceFileRepository.delete(labDeviceFile);
            }
        }
    }
    
    public StreamedContent downloadFile(String path, String fileName) throws IOException {
        StreamedContent file = null;
        try {
            FileInputStream fis = new FileInputStream(path);
            file = new DefaultStreamedContent(fis, FilenameUtils.getExtension(path), fileName);
        } catch (FileNotFoundException ex) {
        }
        
        return file;
    }
    
    public void upFile(UploadedFile file, LabDevice labDevice) throws IOException, Exception {
        Boolean save = true;
        File dir = new File("LabDeviceFiles"); 
        if (dir.exists() == false) dir.mkdir();
        String path = "LabDeviceFiles/" + labDevice.getId() + "_" + file.getFileName();
        if(new File(path).exists()){
            File delFile = new File(path);
            if (delFile.delete()) {
                save = true;
            } 
            else {
                save = false;
            }
        }
        if(save == true){
            try
            {
                file.write(path);
            } catch (Exception e)
            {
                System.out.println("saving of file failed");
                e.printStackTrace();
            }
            LabDeviceFile labDeviceFile = labDeviceFileRepository.getLabDeviceFileByNameAndLabDevice(path, labDevice);
            if(labDeviceFile == null) labDeviceFile = new LabDeviceFile();
            if(labDeviceFile.getId() == null){
                labDeviceFile.setLabDeviceTag(labDevice);
                labDeviceFile.setFileName(file.getFileName());
                labDeviceFile.setPath(path);
                labDeviceFileRepository.save(labDeviceFile);
            }
        }
    }
}
