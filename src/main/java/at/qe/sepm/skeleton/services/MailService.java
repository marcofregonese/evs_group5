/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package at.qe.sepm.skeleton.services;

import java.util.Properties;
import javax.mail.Authenticator;
import javax.mail.Message;
import javax.mail.MessagingException;
import javax.mail.PasswordAuthentication;
import javax.mail.Session;
import javax.mail.Transport;
import javax.mail.internet.InternetAddress;
import javax.mail.internet.MimeMessage;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.context.annotation.Scope;
import org.springframework.stereotype.Component;

/**
 *
 * @author Manuel
 */
@Component
@Scope("application")
public class MailService {
    
    @Autowired
    private SettingService settingService;
    
    private static final Logger logger = LoggerFactory.getLogger(SchedulerService.class);
    
    public void SendMail(String receiver, String subject, String mailtext) {
        try {
            
            String server = settingService.getBySettingName("MAIL:SERVER").getSettingValue();
            String port = settingService.getBySettingName("MAIL:PORT").getSettingValue();
            String ssl = settingService.getBySettingName("MAIL:SSL").getSettingValue();
            String starttls = settingService.getBySettingName("MAIL:STARTTLS").getSettingValue();
            String username = settingService.getBySettingName("MAIL:USERNAME").getSettingValue();
            String password = settingService.getBySettingName("MAIL:PASSWORD").getSettingValue();
            String sender = settingService.getBySettingName("MAIL:SENDER").getSettingValue();
            
            Properties props = new Properties();
            props.setProperty("mail.smtp.host", server);
            props.setProperty("mail.smtp.port", port);
            props.setProperty("mail.smtp.auth", "true");
            props.setProperty("mail.smtp.ssl.enable", ssl);
            props.setProperty("mail.smtp.starttls.enable", starttls);
            
            Session session = Session.getInstance(props);
            
            MimeMessage message = new MimeMessage(session);
            message.setFrom(new InternetAddress(sender));
            message.setRecipients(Message.RecipientType.TO, InternetAddress.parse(receiver));
            message.setSubject(subject);
            message.setText(mailtext);

            Transport tr = session.getTransport("smtps");
            tr.connect(server, Integer.valueOf(port), username, password);
            message.saveChanges();
            tr.sendMessage(message, message.getAllRecipients());
            tr.close();

        } catch (MessagingException e) {
            logger.error("Cannot send mail: " + e.toString() );
        }
    }
    
    private class SMTPAuthenticator extends Authenticator {

        private PasswordAuthentication authentication;

        public SMTPAuthenticator(String login, String password) {
            authentication = new PasswordAuthentication(login, password);
        }

        protected PasswordAuthentication getPasswordAuthentication() {
            return authentication;
        }
    }
}
