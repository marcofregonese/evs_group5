/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package at.qe.sepm.skeleton.services;

import at.qe.sepm.skeleton.model.Laboratory;
import at.qe.sepm.skeleton.model.OpeningTime;
import at.qe.sepm.skeleton.repositories.OpeningTimeRepository;
import java.util.List;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.context.annotation.Scope;
import org.springframework.stereotype.Component;

/**
 *
 * @author Manuel
 */
@Component
@Scope("application")
public class OpeningTimeService {
    
    @Autowired
    OpeningTimeRepository openingTimeRepository;
    
    public List<OpeningTime> getAllByLaboratoryTag(Laboratory laboratory){
        return openingTimeRepository.findAllForLaboraty(laboratory);
    }
    
    public OpeningTime getById(Integer id){
        return openingTimeRepository.findFirstById(id);
    }
    
    public OpeningTime save(OpeningTime openingTime){
        return openingTimeRepository.save(openingTime);
    }
    
    public void delete (OpeningTime openingTime){
        openingTimeRepository.delete(openingTime);
    }
    
    public OpeningTime getOpeningTime(Laboratory laboratory, String day){
        OpeningTime ot =  openingTimeRepository.findDayForLaboraty(laboratory, day);
        if(ot == null){ 
            ot = new OpeningTime();
            ot.setLaboratoryTag(laboratory);
            ot.setDayOfWeek(day);
            return ot; 
        }
        else return ot;
    }
}
