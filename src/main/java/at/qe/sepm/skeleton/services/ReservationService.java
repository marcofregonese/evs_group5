package at.qe.sepm.skeleton.services;

import at.qe.sepm.skeleton.model.Holiday;
import at.qe.sepm.skeleton.model.LabDevice;
import at.qe.sepm.skeleton.model.Laboratory;
import at.qe.sepm.skeleton.model.OpeningTime;
import at.qe.sepm.skeleton.model.Reservation;
import at.qe.sepm.skeleton.repositories.HolidayRepository;
import at.qe.sepm.skeleton.repositories.LaboratoryRepository;
import at.qe.sepm.skeleton.repositories.ReservationRepository;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.context.annotation.Scope;
import org.springframework.stereotype.Component;

import java.time.*;
import java.util.List;

import java.util.ArrayList;
import java.util.Date;
import java.util.stream.Collectors;

/**
 * Created by ${Marc} on ${05.12.2018}.
 */

@Component
@Scope("application")
public class ReservationService {

    @Autowired
    private ReservationRepository reservationRepository;

    @Autowired
    private HolidayRepository holidayRepository;
    
    @Autowired
    private LaboratoryRepository laboratoryRepository;

    public List<Reservation> getAllReservations(){
        return reservationRepository.getAllReservations();
    }
    
    public List<Reservation> getReservations(){
        return reservationRepository.findAll();
    }
    
    public List<Reservation> getReservationsFromUsername(String username){
        return reservationRepository.getReservationsFromUsername(username);
    }
    
    public List<Reservation> getAllReservationsFromUsername(String username) {
        return reservationRepository.getAllReservationsFromUsername(username);
    }
    
    public List<Reservation> getAllActiveReservationsFromLabDevice(Integer labDeviceId){
        return reservationRepository.getAllActiveReservationFromLabDeviceId(labDeviceId);
    }
    
    public boolean checkIfDateDisruptNoReservations(LocalDate date, LocalTime time, LabDevice labDevice){
        return reservationRepository.getAllDisruptedReservationsByDate(date, time, labDevice.getId()).isEmpty();
    }
    
    public boolean isReservationBetweenDateSpan(LocalDate startDate, LocalTime startTime, LocalDate endDate, LocalTime endTime, Integer labDeviceId){
        return !reservationRepository.getAllDiruptedReservationsByDateSpan(startDate, startTime, endDate, endTime, labDeviceId).isEmpty();
    }

    public boolean checkIfDatesAndTimesAreValid (LocalDate startDate, LocalDate endDate, LocalTime startTime, LocalTime endTime) {
        if ((startDate.isBefore(LocalDate.now()) && (!startDate.isEqual(LocalDate.now())) || endDate.isBefore(startDate))) {
            return false;
        }
        if (startDate.isEqual(LocalDate.now())) {
            if (startTime.isBefore(LocalTime.now().minusMinutes(2))) {
                return false;
            }
        }

        if (startDate.isEqual(endDate)) {
            if (startTime.isBefore(endTime)) {
                return true;
            } else {
                return false;
            }
        } else {
            return true;
        }
    }

    public boolean checkIfDateIsNotAHoliday(LocalDate localDate) {
        List<Holiday> holidays = holidayRepository.findAll();
        for (int i = 0; i < holidays.size(); i++) {
            LocalDate ld = LocalDateTime.ofInstant(holidays.get(i).getDate().toInstant(), ZoneId.of("CET")).toLocalDate();
            if(ld.equals(localDate)) return false;
        }
        return true;
    }

    public Reservation getReservationById(long id) {
        return reservationRepository.findFirstById(id);
    }

    public Reservation saveReservation(Reservation reservation) {
        return reservationRepository.save(reservation);
    }

    public void deleteReservation(Reservation reservation) {
        reservationRepository.delete(reservation);
    }

    public List<LabDevice> getAllReservationsFromLabDeviceId(Integer id) {
        return reservationRepository.getAllReservationsFromLabDeviceId(id);
    }

    public List<Reservation> getAllActiveReservations(){
    	return reservationRepository.getAllActiveReservations();
    }
    
    public List<Reservation> getAllActiveReservationsFromUsername(String username){
    	return reservationRepository.getAllActiveReservationsFromUsername(username);
    }
    
    public List<Reservation> getAllActiveReservationsByLabDeviceGroupId(Integer labDeviceGroupId){
        //Because H2 cant group by --> http://www.h2database.com/html/grammar.html
        List<Reservation> reservations = reservationRepository.getAllActiveReservationIdsByLabDeviceGroupId(labDeviceGroupId);
        List<Reservation> ret = new ArrayList<>();
        for(Reservation reservation : reservations){
            if(ret.stream().filter(x->x.getEndDate().equals(reservation.getEndDate()) && x.getEndTime().equals(reservation.getEndTime())).count() == 0)
                ret.add(reservation);
        }
        return ret;
    }
    
    public List<LabDevice> getReservableLabDevicesOfModel(LocalDate startDate, LocalDate endDate, LocalTime startTime, LocalTime endTime, String labDeviceModel) {
        return reservationRepository.getReservableLabDevicesOfModel(startDate, endDate, startTime, endTime, LocalTime.MIN, LocalTime.MIDNIGHT, labDeviceModel);
    }
    
    public int getReservationCount() {
        return reservationRepository.findAll().size();
    }
    
    public List<LabDevice> getReservableLabDevices(Date startDate, Date endDate, Integer minMaxDuration){
        LocalDateTime startLdt = LocalDateTime.ofInstant(startDate.toInstant(), ZoneId.of("CET"));
        LocalDateTime endLdt = LocalDateTime.ofInstant(endDate.toInstant(), ZoneId.of("CET"));
        List<LabDevice> labDevices = new ArrayList<>();
        List<Laboratory> labs = laboratoryRepository.findAll();
        for(Laboratory lab : labs){
            OpeningTime startOt = lab.getOpeningTimes().stream().filter(x->x.getDayOfWeek().equals(getDayOfDayOfWeek(startLdt.getDayOfWeek()))).findFirst().orElse(null);
            OpeningTime endOt = lab.getOpeningTimes().stream().filter(x->x.getDayOfWeek().equals(getDayOfDayOfWeek(endLdt.getDayOfWeek()))).findFirst().orElse(null);
            if(startOt != null && endOt != null){
                LocalTime otStartOpenLt = LocalDateTime.ofInstant(startOt.getOpenTime().toInstant(), ZoneId.of("CET")).toLocalTime();
                LocalTime otStartCloseLt = LocalDateTime.ofInstant(startOt.getCloseTime().toInstant(), ZoneId.of("CET")).toLocalTime();
                LocalTime otEndOpenLt = LocalDateTime.ofInstant(endOt.getOpenTime().toInstant(), ZoneId.of("CET")).toLocalTime();
                LocalTime otEndCloseLt = LocalDateTime.ofInstant(endOt.getCloseTime().toInstant(), ZoneId.of("CET")).toLocalTime();
                if((startLdt.toLocalTime().isAfter(otStartOpenLt) || startLdt.toLocalTime().equals(otStartOpenLt)) &&
                        startLdt.toLocalTime().isBefore(otStartCloseLt) &&
                        (endLdt.toLocalTime().isAfter(otEndOpenLt) || endLdt.toLocalTime().equals(otEndOpenLt)) &&
                        endLdt.toLocalTime().isBefore(otEndCloseLt)){
                    List<LabDevice> tempLabDevices = lab.getLabDevices().stream().filter(x->x.getMaxDurationInHours() >= minMaxDuration && x.isState() == true).collect(Collectors.toList());
                    for(LabDevice labDevice : tempLabDevices){
                        if(isReservationBetweenDateSpan(startLdt.toLocalDate(), startLdt.toLocalTime(), endLdt.toLocalDate(), endLdt.toLocalTime(), labDevice.getId()) == false){
                            labDevices.add(labDevice);
                        } 
                    }
                }
            }
        }
        return labDevices;
    }
    
    public List<Reservation> getAllReservationsForReminderMail(){
        LocalDateTime startLdt = LocalDateTime.ofInstant(new Date().toInstant(), ZoneId.of("CET")).plusDays(1);
        startLdt = startLdt.withSecond(0);
        startLdt = startLdt.withNano(0);
        LocalDateTime endLdt = startLdt.plusDays(3);
        List<Reservation> reservations = reservationRepository.getAllReservationsForReminderMail(startLdt.toLocalTime(), startLdt.toLocalDate(), endLdt.toLocalTime(), endLdt.toLocalDate());
        List<Reservation> ret = new ArrayList<>();
        for(Reservation reservation : reservations){
            if(ret.stream().filter(x->x.getStartDate().equals(reservation.getStartDate()) 
                    && x.getStartTime().equals(reservation.getStartTime())
                    && x.getEndDate().equals(reservation.getEndDate()) 
                    && x.getEndTime().equals(reservation.getEndTime())
                    && x.getUserTag().equals(reservation.getUserTag())
                    && x.getLabDeviceGroup() == reservation.getLabDeviceGroup()).count() == 0)
                ret.add(reservation);
        }
        return ret;
    }
    
    public List<Reservation> getAllOverdueReservations(){
        LocalDateTime ldt = LocalDateTime.ofInstant(new Date().toInstant(), ZoneId.of("CET"));
        List<Reservation> reservations = reservationRepository.getAllOverdueReservationsFromDate(ldt.toLocalDate(), ldt.toLocalTime());
        List<Reservation> ret = new ArrayList<>();
        for(Reservation reservation : reservations){
            if(ret.stream().filter(x->x.getStartDate().equals(reservation.getStartDate()) 
                    && x.getStartTime().equals(reservation.getStartTime())
                    && x.getEndDate().equals(reservation.getEndDate()) 
                    && x.getEndTime().equals(reservation.getEndTime())
                    && x.getUserTag().equals(reservation.getUserTag())
                    && x.getLabDeviceGroup() == reservation.getLabDeviceGroup()).count() == 0)
                ret.add(reservation);
        }
        return ret;
    }
    
    public List<Reservation> getAllReservationsFromGroupReservation(LocalDate startDate, LocalTime startTime, LocalDate endDate, LocalTime endTime, String username, Integer labDeviceGroupId){
        return reservationRepository.getAllReservationsFromGroupReservation(startTime, startDate, endTime, endDate, username, labDeviceGroupId);
    }
    
    private String getDayOfDayOfWeek(DayOfWeek day){
        switch (day){
            case MONDAY:
                return "MON";
            case TUESDAY:
                return "TUE";
            case WEDNESDAY:
                return "WED";
            case THURSDAY:
                return "THU";
            case FRIDAY:
                return "FRI";
        }
        return "";
    }
}
