/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package at.qe.sepm.skeleton.services;

import at.qe.sepm.skeleton.model.Laboratory;
import at.qe.sepm.skeleton.repositories.LaboratoryRepository;
import java.util.List;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.context.annotation.Scope;
import org.springframework.stereotype.Component;

/**
 *
 * @author Manuel
 */
@Component
@Scope("application")
public class LaboratoryService {
    
    @Autowired
    LaboratoryRepository laboratoryRepository;
    
    public List<Laboratory> getAllLaboratories(){
        return laboratoryRepository.findAll();
    } 
    
    public Laboratory getLaboratoryById(Integer id){
        return laboratoryRepository.findFirstById(id);
    }
    
    public Laboratory getLaborytorybyName(String laboratoryName){
        return laboratoryRepository.findFirstByName(laboratoryName);
    }
    
    public List<Laboratory> getAllLaborytorybyName(String laboratoryName){
        return laboratoryRepository.findAllByName(laboratoryName);
    }
    
    public Laboratory saveLaboratory(Laboratory laboratory) {
        return laboratoryRepository.save(laboratory);
    }

    public void deleteLaboratory(Laboratory laboratory) {
        laboratoryRepository.delete(laboratory);
    }
}
