/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package at.qe.sepm.skeleton.services;

import at.qe.sepm.skeleton.model.Setting;
import at.qe.sepm.skeleton.repositories.SettingRepository;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.context.annotation.Scope;
import org.springframework.stereotype.Component;

/**
 *
 * @author Manuel
 */
@Component
@Scope("application")
public class SettingService {
    
    @Autowired
    SettingRepository settingRepository;
    
    public Setting getBySettingName(String settingName){
        return settingRepository.findFirstBySettingName(settingName);
    }
    
    public Setting save(Setting setting){
        return settingRepository.save(setting);
    }
    
    public void delete(Setting setting){
        settingRepository.delete(setting);
    }
}
