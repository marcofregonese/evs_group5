package at.qe.sepm.skeleton.services;

import at.qe.sepm.skeleton.model.LabDeviceGroup;
import at.qe.sepm.skeleton.model.User;
import at.qe.sepm.skeleton.repositories.LabDeviceGroupRepository;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.context.annotation.Scope;
import org.springframework.stereotype.Component;

import java.util.List;

/**
 * Created by ${Marc} on ${21.10.2016}.
 */

@Component
@Scope("application")
public class LabDeviceGroupService {

    @Autowired
    LabDeviceGroupRepository labDeviceGroupRepository;

    public List<LabDeviceGroup> getLabDeviceGroupsOfUser(User user) {
        return labDeviceGroupRepository.getLabDeviceGroupsOfUser(user.getUsername());
    }

    public List<String> getDeviceModelsOfGroup(Integer id) {
        return labDeviceGroupRepository.getDeviceModelsOfGroup(id);
    }

    public LabDeviceGroup saveLabDeviceGroup(LabDeviceGroup labDeviceGroup) {
        return labDeviceGroupRepository.save(labDeviceGroup);
    }

    public void deleteLabDeviceGroup(LabDeviceGroup labDeviceGroup) {
        labDeviceGroupRepository.delete(labDeviceGroup);
    }

    public List<LabDeviceGroup> getAllLabDeviceGroups(){
        return labDeviceGroupRepository.findAll();
    }
    
    public LabDeviceGroup getLabDeviceGroupByName(String name){
        return labDeviceGroupRepository.getLabDeviceGroupByName(name);
    }
    
    public LabDeviceGroup getLabDeviceGroupById(Integer id){
        return labDeviceGroupRepository.findOne(id);
    }
}
