/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package at.qe.sepm.skeleton.services;

import at.qe.sepm.skeleton.model.Holiday;
import at.qe.sepm.skeleton.repositories.HolidayRepository;
import java.util.List;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.context.annotation.Scope;
import org.springframework.stereotype.Component;

/**
 *
 * @author Manuel
 */
@Component
@Scope("application")
public class HolidayService {
    
    @Autowired
    HolidayRepository holidayRepository;

    public List<Holiday> getAllHolidays() {
        return holidayRepository.findAll();
    }

    public Holiday saveHoliday(Holiday holiday) {
        return holidayRepository.save(holiday);
    }
    
    public void deleteHoliday(Holiday holiday){
        holidayRepository.delete(holiday);
    }
}
