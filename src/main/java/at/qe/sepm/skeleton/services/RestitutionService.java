/**
 * 
 */
package at.qe.sepm.skeleton.services;

import java.util.Date;
import java.util.List;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.context.annotation.Scope;
import org.springframework.stereotype.Component;

import at.qe.sepm.skeleton.model.Reservation;
import at.qe.sepm.skeleton.repositories.ReservationRepository;
import java.time.LocalDateTime;
import java.time.ZoneId;

/**
 * @author marco
 *
 */

@Component
@Scope("application")
public class RestitutionService {

    @Autowired
    private ReservationRepository reservationRepository;
    
    public List<Reservation> getAllRestitutions(){
    	return reservationRepository.getAllRestitutions();
    }
    
    public List<Reservation> getAllRestitutionsFromUsername(String username){
    	return reservationRepository.getAllRestitutionsFromUsername(username);
    }
    
    public Reservation confirmRestitution(Reservation reservation, String adminUsername) {
    	reservation.setAdminUsername(adminUsername);
    	reservation.setRestitutionDate(new Date());
        return reservationRepository.save(reservation);
    }
    
    public Reservation unconfirmRestitution(Reservation reservation) {
    	reservation.setAdminUsername(null);
    	reservation.setRestitutionDate(null);
    	return reservationRepository.save(reservation);
    }
    
    public List<Reservation> getAllActiveRestitutionsFromUsername(String username){
        LocalDateTime ldt = LocalDateTime.ofInstant((new Date()).toInstant(), ZoneId.of("CET"));
        return reservationRepository.getAllActiveRestitutionsFromUsername(username, ldt.toLocalDate(), ldt.toLocalTime());
    }
    
    public List<Reservation> getAllActiveRestitutions(){
        LocalDateTime ldt = LocalDateTime.ofInstant((new Date()).toInstant(), ZoneId.of("CET"));
        return reservationRepository.getAllActiveRestitutions(ldt.toLocalDate(), ldt.toLocalTime());
    }
}
